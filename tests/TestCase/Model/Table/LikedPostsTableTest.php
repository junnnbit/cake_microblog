<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LikedPostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LikedPostsTable Test Case
 */
class LikedPostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LikedPostsTable
     */
    protected $LikedPosts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.LikedPosts',
        'app.Users',
        'app.Posts1',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('LikedPosts') ? [] : ['className' => LikedPostsTable::class];
        $this->LikedPosts = $this->getTableLocator()->get('LikedPosts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->LikedPosts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\LikedPostsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\LikedPostsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
