-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2021 at 06:29 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `area` varchar(500) NOT NULL,
  `city` varchar(250) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `area`, `city`, `is_active`, `added_on`, `added_by`) VALUES
(1, 'MAIN BRANCH', 'MAIN BRANCH', 'MAIN BRANCH', 1, '2021-09-25 22:28:48', 1),
(2, 'Liceo de San Pedro', 'San Pedro', 'Laguna', 1, '2021-09-26 16:55:37', 1),
(3, 'Capinpin, Muntinlupa', 'Putatan', 'Muntinlupa', 1, '2021-09-26 17:25:22', 1),
(4, 'Bayanan, Muntinlupa', 'Putatan', 'Muntinlupa', 1, '2021-09-26 17:28:37', 1),
(5, 'Mariquita', 'Santa Rosa', 'Laguna', 1, '2021-10-07 22:18:48', 1),
(6, 'Crosstown Mall', 'Santa Rosa', 'Laguna', 1, '2021-10-07 22:27:40', 1),
(7, 'Arabella', 'Cabuyao', 'Laguna', 1, '2021-10-07 22:30:07', 1),
(8, 'Mercado De Calamba', 'Calamba', 'Laguna', 1, '2021-10-07 22:30:40', 1),
(9, 'Marick', 'Cainta', 'Rizal', 1, '2021-10-07 22:32:43', 1),
(10, 'Anchormans Food Hub', 'Liliw', 'Laguna', 0, '2021-10-07 22:34:22', 1),
(11, 'Ligtasan', 'Antipolo', 'Rizal', 1, '2021-10-07 22:36:41', 1),
(12, 'Our Lady of Fatima University', 'Valenzuela', 'Valenzuela', 1, '2021-10-07 22:38:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  `remarks` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_info_id`, `remarks`, `description`, `added_on`) VALUES
(1, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:04:31'),
(2, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:06:46'),
(3, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:07:23'),
(4, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:08:17'),
(5, 1, 'LOG', 'LOGGED OUT', '2021-10-10 01:09:10'),
(6, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:09:20'),
(7, 1, 'LOG', 'LOGGED OUT', '2021-10-10 01:09:23'),
(8, 1, 'LOG', 'LOGGED IN', '2021-10-10 01:09:38'),
(9, 1, 'LOG', 'LOGGED IN', '2021-10-10 18:52:08'),
(10, 1, 'LOG', 'LOGGED OUT', '2021-10-10 19:05:03'),
(11, 4, 'LOG', 'LOGGED IN', '2021-10-10 19:05:09'),
(12, 4, 'LOG', 'LOGGED OUT', '2021-10-10 19:05:14'),
(13, 1, 'LOG', 'LOGGED IN', '2021-10-10 19:05:18'),
(14, 1, 'LOG', 'LOGGED IN', '2021-10-11 09:15:25'),
(15, 1, 'LOG', 'LOGGED IN', '2021-10-11 17:50:14'),
(16, 1, 'LOG', 'LOGGED IN', '2021-10-11 22:14:19'),
(17, 5, 'LOG', 'LOGGED IN', '2021-10-16 21:09:56'),
(18, 5, 'LOG', 'LOGGED IN', '2021-10-17 13:11:54'),
(19, 5, 'LOG', 'LOGGED OUT', '2021-10-17 14:07:14'),
(20, 1, 'LOG', 'LOGGED IN', '2021-10-17 14:07:19'),
(21, 1, 'LOG', 'LOGGED OUT', '2021-10-17 14:07:25'),
(22, 5, 'LOG', 'LOGGED IN', '2021-10-17 14:07:33'),
(23, 5, 'LOG', 'LOGGED OUT', '2021-10-17 14:25:35'),
(24, 7, 'LOG', 'LOGGED IN', '2021-10-17 14:25:42'),
(25, 7, 'LOG', 'LOGGED OUT', '2021-10-17 14:27:54'),
(26, 5, 'LOG', 'LOGGED IN', '2021-10-17 14:28:05'),
(27, 5, 'LOG', 'LOGGED OUT', '2021-10-17 16:26:05'),
(28, 1, 'LOG', 'LOGGED IN', '2021-10-17 16:26:12'),
(29, 1, 'LOG', 'LOGGED OUT', '2021-10-17 16:26:41'),
(30, 5, 'LOG', 'LOGGED IN', '2021-10-17 16:26:50'),
(31, 5, 'LOG', 'LOGGED OUT', '2021-10-17 21:01:12'),
(32, 1, 'LOG', 'LOGGED IN', '2021-10-17 21:01:17'),
(33, 1, 'LOG', 'LOGGED OUT', '2021-10-17 21:01:34'),
(34, 5, 'LOG', 'LOGGED IN', '2021-10-17 21:03:02'),
(35, 5, 'LOG', 'LOGGED OUT', '2021-10-17 23:42:12'),
(36, 1, 'LOG', 'LOGGED IN', '2021-10-17 23:42:18'),
(37, 1, 'LOG', 'LOGGED OUT', '2021-10-17 23:45:06'),
(38, 7, 'LOG', 'LOGGED IN', '2021-10-17 23:45:19'),
(39, 7, 'LOG', 'LOGGED OUT', '2021-10-17 23:45:24'),
(40, 5, 'LOG', 'LOGGED IN', '2021-10-17 23:45:31'),
(41, 5, 'LOG', 'LOGGED OUT', '2021-10-18 00:03:39'),
(42, 5, 'LOG', 'LOGGED IN', '2021-10-18 19:37:52'),
(43, 5, 'LOG', 'LOGGED OUT', '2021-10-18 20:37:29'),
(44, 3, 'LOG', 'LOGGED IN', '2021-10-18 20:37:40'),
(45, 3, 'LOG', 'LOGGED OUT', '2021-10-18 22:03:23'),
(46, 3, 'LOG', 'LOGGED IN', '2021-10-18 22:03:40'),
(47, 5, 'LOG', 'LOGGED IN', '2021-10-18 22:03:52'),
(48, 5, 'LOG', 'LOGGED OUT', '2021-10-18 23:06:43'),
(49, 3, 'LOG', 'LOGGED IN', '2021-10-18 23:06:50'),
(50, 3, 'LOG', 'LOGGED OUT', '2021-10-18 23:12:04'),
(51, 1, 'LOG', 'LOGGED IN', '2021-10-18 23:12:09'),
(52, 1, 'LOG', 'LOGGED OUT', '2021-10-19 00:07:27'),
(53, 5, 'LOG', 'LOGGED IN', '2021-10-19 00:07:37'),
(54, 3, 'LOG', 'LOGGED IN', '2021-10-19 21:32:21'),
(55, 3, 'LOG', 'LOGGED OUT', '2021-10-19 22:27:08'),
(56, 5, 'LOG', 'LOGGED IN', '2021-10-19 22:27:16'),
(57, 5, 'LOG', 'LOGGED OUT', '2021-10-19 22:31:41'),
(58, 3, 'LOG', 'LOGGED IN', '2021-10-19 22:31:47'),
(59, 3, 'LOG', 'LOGGED IN', '2021-10-20 06:32:23'),
(60, 3, 'LOG', 'LOGGED IN', '2021-10-20 08:48:11'),
(61, 3, 'LOG', 'LOGGED OUT', '2021-10-20 10:14:22'),
(62, 3, 'LOG', 'LOGGED IN', '2021-10-20 13:41:17'),
(63, 3, 'LOG', 'LOGGED OUT', '2021-10-20 20:52:37'),
(64, 5, 'LOG', 'LOGGED IN', '2021-10-20 20:52:47'),
(65, 5, 'LOG', 'LOGGED OUT', '2021-10-20 20:56:48'),
(66, 3, 'LOG', 'LOGGED IN', '2021-10-20 20:56:56'),
(67, 3, 'LOG', 'LOGGED OUT', '2021-10-20 20:58:41'),
(68, 5, 'LOG', 'LOGGED IN', '2021-10-20 20:59:17'),
(69, 5, 'LOG', 'LOGGED OUT', '2021-10-20 21:03:13'),
(70, 3, 'LOG', 'LOGGED IN', '2021-10-20 21:03:21'),
(71, 3, 'LOG', 'LOGGED OUT', '2021-10-20 21:49:38'),
(72, 5, 'LOG', 'LOGGED IN', '2021-10-20 21:49:48'),
(73, 5, 'LOG', 'LOGGED OUT', '2021-10-20 21:51:10'),
(74, 3, 'LOG', 'LOGGED IN', '2021-10-20 21:51:18'),
(75, 3, 'LOG', 'LOGGED OUT', '2021-10-20 22:02:46'),
(76, 5, 'LOG', 'LOGGED IN', '2021-10-20 22:02:54'),
(77, 5, 'LOG', 'LOGGED OUT', '2021-10-20 22:03:29'),
(78, 3, 'LOG', 'LOGGED IN', '2021-10-20 22:03:35'),
(79, 3, 'LOG', 'LOGGED OUT', '2021-10-20 22:13:38'),
(80, 3, 'LOG', 'LOGGED IN', '2021-10-20 23:04:03'),
(81, 5, 'LOG', 'LOGGED IN', '2021-10-22 19:04:53'),
(82, 5, 'LOG', 'LOGGED OUT', '2021-10-22 19:05:14'),
(83, 3, 'LOG', 'LOGGED IN', '2021-10-22 19:05:23'),
(84, 1, 'LOG', 'LOGGED IN', '2021-10-23 11:48:59'),
(85, 1, 'LOG', 'LOGGED OUT', '2021-10-23 22:13:39'),
(86, 3, 'LOG', 'LOGGED IN', '2021-10-23 22:13:46'),
(87, 3, 'LOG', 'LOGGED OUT', '2021-10-23 22:14:13'),
(88, 5, 'LOG', 'LOGGED IN', '2021-10-23 22:14:20'),
(89, 5, 'LOG', 'LOGGED OUT', '2021-10-23 22:14:38'),
(90, 1, 'LOG', 'LOGGED IN', '2021-10-23 22:15:04'),
(91, 1, 'LOG', 'LOGGED OUT', '2021-10-23 22:19:17'),
(92, 5, 'LOG', 'LOGGED IN', '2021-10-23 22:19:27'),
(93, 5, 'LOG', 'LOGGED OUT', '2021-10-23 22:19:48'),
(94, 1, 'LOG', 'LOGGED IN', '2021-10-23 22:19:57'),
(95, 1, 'LOG', 'LOGGED OUT', '2021-10-24 00:04:10'),
(96, 5, 'LOG', 'LOGGED IN', '2021-10-24 00:04:17'),
(97, 5, 'LOG', 'LOGGED OUT', '2021-10-24 00:10:19'),
(98, 1, 'LOG', 'LOGGED IN', '2021-10-24 00:10:29'),
(99, 1, 'LOG', 'LOGGED OUT', '2021-10-24 00:49:40'),
(100, 5, 'LOG', 'LOGGED IN', '2021-10-24 00:49:49'),
(101, 5, 'LOG', 'LOGGED OUT', '2021-10-24 00:51:42'),
(102, 1, 'LOG', 'LOGGED IN', '2021-10-24 00:51:48'),
(103, 1, 'LOG', 'LOGGED OUT', '2021-10-24 01:01:26'),
(104, 5, 'LOG', 'LOGGED IN', '2021-10-24 01:01:32'),
(105, 5, 'LOG', 'LOGGED OUT', '2021-10-24 01:01:42'),
(106, 1, 'LOG', 'LOGGED IN', '2021-10-24 01:01:47'),
(107, 1, 'LOG', 'LOGGED OUT', '2021-10-24 01:03:18'),
(108, 3, 'LOG', 'LOGGED IN', '2021-10-24 01:03:25'),
(109, 3, 'ORDER', 'ORDER #0000000009 MARKED AS PAID', '2021-10-24 02:09:41'),
(110, 3, 'ORDER', 'ORDER #0000000007 MARKED AS IN TRANSIT', '2021-10-24 02:10:03'),
(111, 3, 'LOG', 'LOGGED OUT', '2021-10-24 02:10:37'),
(112, 1, 'LOG', 'LOGGED IN', '2021-10-24 02:10:45'),
(113, 1, 'LOG', 'LOGGED OUT', '2021-10-24 02:14:00'),
(114, 5, 'LOG', 'LOGGED IN', '2021-10-24 02:14:14'),
(115, 5, 'ORDER', 'ORDER PLACED', '2021-10-24 02:14:27'),
(116, 5, 'LOG', 'LOGGED OUT', '2021-10-24 02:14:42'),
(117, 1, 'LOG', 'LOGGED IN', '2021-10-24 02:14:49'),
(118, 1, 'LOG', 'LOGGED OUT', '2021-10-24 02:29:07'),
(119, 5, 'LOG', 'LOGGED IN', '2021-10-24 02:29:14'),
(120, 1, 'LOG', 'LOGGED IN', '2021-10-24 15:06:15'),
(121, 1, 'LOG', 'LOGGED OUT', '2021-10-24 21:56:16'),
(122, 5, 'LOG', 'LOGGED IN', '2021-10-24 21:56:23'),
(123, 5, 'LOG', 'LOGGED OUT', '2021-10-24 21:59:31'),
(124, 5, 'LOG', 'LOGGED IN', '2021-10-24 21:59:38'),
(125, 5, 'LOG', 'LOGGED OUT', '2021-10-24 21:59:42'),
(126, 1, 'LOG', 'LOGGED IN', '2021-10-24 21:59:46'),
(127, 1, 'LOG', 'LOGGED OUT', '2021-10-24 21:59:54'),
(128, 5, 'LOG', 'LOGGED IN', '2021-10-25 06:54:53'),
(129, 5, 'LOG', 'LOGGED OUT', '2021-10-25 07:22:22'),
(130, 1, 'LOG', 'LOGGED IN', '2021-10-25 20:04:45'),
(131, 1, 'ORDER', 'ORDER #0000000006 MARKED AS FOR RETURN', '2021-10-25 20:05:03'),
(132, 1, 'LOG', 'LOGGED OUT', '2021-10-25 21:14:21'),
(133, 1, 'LOG', 'LOGGED IN', '2021-10-26 20:46:21'),
(134, 1, 'LOG', 'LOGGED IN', '2021-10-27 19:16:46'),
(135, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:21:19'),
(136, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:22:19'),
(137, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:23:14'),
(138, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:27:03'),
(139, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:28:17'),
(140, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:28:57'),
(141, 1, 'ORDER', 'ORDER #0000000010 MARKED AS ORDERED', '2021-10-27 20:29:17'),
(142, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:29:56'),
(143, 1, 'ORDER', 'ORDER #0000000010 MARKED AS ORDERED', '2021-10-27 20:30:13'),
(144, 1, 'ORDER', 'ORDER #0000000010 MARKED AS FOR PREPARATION', '2021-10-27 20:30:21'),
(145, 1, 'ORDER', 'ORDER #0000000009 MARKED AS IN TRANSIT', '2021-10-27 20:31:02'),
(146, 1, 'LOG', 'LOGGED OUT', '2021-10-27 20:31:17'),
(147, 5, 'LOG', 'LOGGED IN', '2021-10-27 20:31:26'),
(148, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:32:11'),
(149, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:32:11'),
(150, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:33:07'),
(151, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:33:07'),
(152, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:34:51'),
(153, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:36:46'),
(154, 5, 'ORDER', 'ORDER PLACED', '2021-10-27 20:39:14'),
(155, 5, 'ORDER', 'ORDER #0000000009 MARKED AS DELIVERED', '2021-10-27 20:41:18'),
(156, 5, 'LOG', 'LOGGED OUT', '2021-10-27 20:41:36'),
(157, 1, 'LOG', 'LOGGED IN', '2021-10-27 20:41:45'),
(158, 1, 'ORDER', 'ORDER #0000000015 MARKED AS CANCELLED', '2021-10-27 20:41:56'),
(159, 1, 'LOG', 'LOGGED OUT', '2021-10-27 21:46:04'),
(160, 5, 'LOG', 'LOGGED IN', '2021-10-27 21:46:11'),
(161, 5, 'LOG', 'LOGGED OUT', '2021-10-27 21:46:53'),
(162, 1, 'LOG', 'LOGGED IN', '2021-10-27 22:10:53'),
(163, 1, 'LOG', 'LOGGED OUT', '2021-10-27 22:41:39'),
(164, 5, 'LOG', 'LOGGED IN', '2021-10-27 22:41:45'),
(165, 5, 'LOG', 'LOGGED OUT', '2021-10-27 22:48:17'),
(166, 3, 'LOG', 'LOGGED IN', '2021-10-28 06:10:19'),
(167, 3, 'LOG', 'LOGGED OUT', '2021-10-28 06:10:45'),
(168, 5, 'LOG', 'LOGGED IN', '2021-10-28 06:10:52'),
(169, 5, 'ORDER', 'ORDER #0000000009 MARKED AS FOR RETURN', '2021-10-28 06:11:04'),
(170, 5, 'LOG', 'LOGGED OUT', '2021-10-28 06:11:08'),
(171, 3, 'LOG', 'LOGGED IN', '2021-10-28 06:11:19'),
(172, 3, 'LOG', 'LOGGED OUT', '2021-10-28 06:13:00'),
(173, 1, 'LOG', 'LOGGED IN', '2021-10-28 06:13:10'),
(174, 1, 'ORDER', 'ORDER #0000000006 MARKED AS FOR RETURN', '2021-10-28 06:13:25'),
(175, 1, 'LOG', 'LOGGED OUT', '2021-10-28 06:19:21'),
(176, 5, 'LOG', 'LOGGED IN', '2021-10-28 06:19:28'),
(177, 5, 'ORDER', 'ORDER PLACED', '2021-10-28 06:20:39'),
(178, 5, 'LOG', 'LOGGED OUT', '2021-10-28 06:20:43'),
(179, 3, 'LOG', 'LOGGED IN', '2021-10-28 06:20:51'),
(180, 3, 'ORDER', 'ORDER #0000000011 MARKED AS FOR PREPARATION', '2021-10-28 06:21:47'),
(181, 3, 'LOG', 'LOGGED OUT', '2021-10-28 06:22:09'),
(182, 5, 'LOG', 'LOGGED IN', '2021-10-28 06:22:18'),
(183, 5, 'LOG', 'LOGGED OUT', '2021-10-28 06:25:04'),
(184, 3, 'LOG', 'LOGGED IN', '2021-10-28 06:25:11');

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE `notif` (
  `id` int(11) NOT NULL,
  `viewer_id` int(11) NOT NULL,
  `notif_type` varchar(250) NOT NULL,
  `notif_desc` varchar(250) NOT NULL,
  `is_seen` int(11) NOT NULL DEFAULT 0,
  `is_clicked` int(11) NOT NULL DEFAULT 0,
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notif`
--

INSERT INTO `notif` (`id`, `viewer_id`, `notif_type`, `notif_desc`, `is_seen`, `is_clicked`, `added_on`) VALUES
(1, 5, 'ORDER', 'Order with PO#0000000010 is accepted and is now FOR PREPARATION.', 1, 0, '2021-10-28 06:24:04'),
(2, 1, 'ORDER', 'Order with PO#0000000015 was created from Liceo de San Pedro', 1, 0, '2021-10-28 06:24:04'),
(3, 3, 'ORDER', 'Order with PO#0000000015 was created from Liceo de San Pedro', 1, 0, '2021-10-28 06:24:04'),
(4, 4, 'ORDER', 'Order with PO#0000000015 was created from Liceo de San Pedro', 0, 0, '2021-10-28 06:24:04'),
(5, 4, 'ORDER', 'Order with PO#0000000009 was successfully DELIVERED.', 0, 0, '2021-10-28 06:24:04'),
(6, 5, 'ORDER', 'Order with PO#0000000015 is marked as CANCELLED.', 1, 0, '2021-10-28 06:24:04'),
(7, 1, 'ORDER', 'Order with PO#0000000016 was created from Liceo de San Pedro', 0, 0, '2021-10-28 06:24:04'),
(8, 3, 'ORDER', 'Order with PO#0000000016 was created from Liceo de San Pedro', 1, 0, '2021-10-28 06:24:04'),
(9, 4, 'ORDER', 'Order with PO#0000000016 was created from Liceo de San Pedro', 0, 0, '2021-10-28 06:24:04'),
(10, 5, 'ORDER', 'Order with PO#0000000011 is accepted and is now FOR PREPARATION.', 1, 0, '2021-10-28 06:24:04');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

CREATE TABLE `order_list` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_price_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_list`
--

INSERT INTO `order_list` (`id`, `order_id`, `product_price_id`, `qty`, `total_price`) VALUES
(3, 6, 8, 1, 46),
(4, 6, 10, 2, 68),
(5, 7, 8, 1, 46),
(6, 8, 8, 2, 92),
(7, 9, 8, 2, 92),
(8, 10, 8, 2, 92),
(9, 11, 8, 1, 46),
(10, 12, 8, 1, 46),
(11, 13, 8, 1, 46),
(12, 14, 8, 1, 46),
(13, 15, 8, 1, 46),
(14, 16, 8, 1, 46);

-- --------------------------------------------------------

--
-- Table structure for table `order_list_inventory`
--

CREATE TABLE `order_list_inventory` (
  `id` int(11) NOT NULL,
  `order_list_id` int(11) NOT NULL,
  `products_inventory_id` int(11) NOT NULL,
  `transacted_by` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `transacted_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_list_inventory`
--

INSERT INTO `order_list_inventory` (`id`, `order_list_id`, `products_inventory_id`, `transacted_by`, `qty`, `transacted_date`, `updated_by`, `updated_on`) VALUES
(1, 3, 9, 3, 1, '2021-10-20 20:34:56', 3, '2021-10-20 20:41:24'),
(2, 3, 11, 3, 0, '2021-10-20 20:40:14', 3, '2021-10-20 20:41:24'),
(3, 4, 10, 3, 2, '2021-10-20 20:42:38', 3, '2021-10-20 20:42:47'),
(4, 5, 9, 3, 1, '2021-10-20 23:21:44', 3, '2021-10-20 23:36:39'),
(5, 5, 11, 3, 0, '2021-10-20 23:36:25', 3, '2021-10-20 23:36:39'),
(6, 7, 11, 1, 1, '2021-10-24 01:00:28', 1, '2021-10-27 20:30:58'),
(7, 7, 9, 1, 1, '2021-10-27 20:30:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `processed_by` int(11) NOT NULL,
  `status` varchar(100) NOT NULL,
  `date_processed` datetime NOT NULL DEFAULT current_timestamp(),
  `is_closed` int(11) NOT NULL DEFAULT 0,
  `date_closed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `order_id`, `processed_by`, `status`, `date_processed`, `is_closed`, `date_closed`) VALUES
(1, 6, 5, 'ORDERED', '2021-10-17 23:00:29', 1, '2021-10-18 23:31:40'),
(4, 6, 1, 'FOR PREPARATION', '2021-10-18 23:31:40', 1, '2021-10-20 20:50:17'),
(5, 7, 5, 'ORDERED', '2021-10-19 22:27:27', 1, '2021-10-19 22:37:17'),
(6, 7, 3, 'FOR PREPARATION', '2021-10-19 22:37:17', 1, '2021-10-24 02:10:03'),
(7, 6, 3, 'IN TRANSIT', '2021-10-20 20:50:17', 1, '2021-10-20 22:03:00'),
(9, 8, 5, 'ORDERED', '2021-10-20 21:50:57', 1, '2021-10-20 21:51:02'),
(10, 8, 5, 'FOR CANCEL', '2021-10-20 21:51:02', 1, '2021-10-20 22:08:11'),
(11, 6, 5, 'DELIVERED', '2021-10-20 22:03:00', 1, '2021-10-22 19:05:02'),
(12, 8, 3, 'CANCELLED', '2021-10-20 22:08:11', 1, '2021-10-23 22:10:18'),
(13, 6, 5, 'FOR RETURN', '2021-10-22 19:05:02', 1, '2021-10-23 20:23:51'),
(16, 6, 1, 'RETURNED', '2021-10-23 20:23:51', 1, '2021-10-25 20:05:03'),
(17, 8, 1, 'FOR CANCEL', '2021-10-23 22:10:18', 1, '2021-10-23 22:12:31'),
(18, 8, 1, 'ORDERED', '2021-10-23 22:12:31', 1, '2021-10-23 22:12:41'),
(19, 8, 1, 'CANCELLED', '2021-10-23 22:12:41', 0, NULL),
(20, 9, 5, 'ORDERED', '2021-10-24 00:51:38', 1, '2021-10-24 00:52:16'),
(21, 9, 1, 'FOR PREPARATION', '2021-10-24 00:52:16', 1, '2021-10-27 20:31:02'),
(22, 7, 3, 'IN TRANSIT', '2021-10-24 02:10:03', 0, NULL),
(23, 10, 5, 'ORDERED', '2021-10-24 02:14:27', 1, '2021-10-27 20:28:57'),
(24, 6, 1, 'FOR RETURN', '2021-10-25 20:05:03', 1, '2021-10-25 20:08:44'),
(25, 6, 1, 'RETURNED', '2021-10-25 20:08:44', 1, '2021-10-28 06:13:25'),
(31, 10, 1, 'FOR PREPARATION', '2021-10-27 20:28:57', 1, '2021-10-27 20:29:17'),
(32, 10, 1, 'ORDERED', '2021-10-27 20:29:17', 1, '2021-10-27 20:29:56'),
(33, 10, 1, 'FOR PREPARATION', '2021-10-27 20:29:56', 1, '2021-10-27 20:30:13'),
(34, 10, 1, 'ORDERED', '2021-10-27 20:30:13', 1, '2021-10-27 20:30:21'),
(35, 10, 1, 'FOR PREPARATION', '2021-10-27 20:30:21', 0, NULL),
(36, 9, 1, 'IN TRANSIT', '2021-10-27 20:31:02', 1, '2021-10-27 20:41:18'),
(37, 11, 5, 'ORDERED', '2021-10-27 20:32:11', 1, '2021-10-28 06:21:47'),
(38, 12, 5, 'ORDERED', '2021-10-27 20:33:07', 0, NULL),
(39, 13, 5, 'ORDERED', '2021-10-27 20:34:51', 0, NULL),
(40, 14, 5, 'ORDERED', '2021-10-27 20:36:46', 0, NULL),
(41, 15, 5, 'ORDERED', '2021-10-27 20:39:14', 1, '2021-10-27 20:41:56'),
(42, 9, 5, 'DELIVERED', '2021-10-27 20:41:18', 1, '2021-10-28 06:11:04'),
(43, 15, 1, 'CANCELLED', '2021-10-27 20:41:56', 0, NULL),
(44, 9, 5, 'FOR RETURN', '2021-10-28 06:11:04', 1, '2021-10-28 06:12:06'),
(45, 9, 3, 'RETURNED', '2021-10-28 06:12:06', 0, NULL),
(46, 6, 1, 'FOR RETURN', '2021-10-28 06:13:25', 0, NULL),
(47, 16, 5, 'ORDERED', '2021-10-28 06:20:39', 0, NULL),
(48, 11, 3, 'FOR PREPARATION', '2021-10-28 06:21:47', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_tbl`
--

CREATE TABLE `order_tbl` (
  `id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `tracking_no` varchar(500) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `is_paid` int(11) NOT NULL DEFAULT 0,
  `date_complete` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_tbl`
--

INSERT INTO `order_tbl` (`id`, `created_by`, `tracking_no`, `date_created`, `is_paid`, `date_complete`) VALUES
(6, 5, 'PO#0000000001', '2021-10-17 23:00:29', 1, '2021-10-20 00:00:00'),
(7, 5, 'PO#0000000007', '2021-10-19 22:27:27', 1, NULL),
(8, 5, 'PO#0000000008', '2021-10-20 21:50:57', 0, NULL),
(9, 5, 'PO#0000000009', '2021-10-24 00:51:38', 1, '2021-10-27 20:41:18'),
(10, 5, 'PO#0000000010', '2021-10-24 02:14:27', 0, NULL),
(11, 5, 'PO#0000000011', '2021-10-27 20:32:11', 0, NULL),
(12, 5, 'PO#0000000012', '2021-10-27 20:33:07', 0, NULL),
(13, 5, 'PO#0000000013', '2021-10-27 20:34:51', 0, NULL),
(14, 5, 'PO#0000000014', '2021-10-27 20:36:46', 0, NULL),
(15, 5, 'PO#0000000015', '2021-10-27 20:39:14', 0, NULL),
(16, 5, 'PO#0000000016', '2021-10-28 06:20:39', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pcp_inventory`
--

CREATE TABLE `pcp_inventory` (
  `id` int(11) NOT NULL,
  `product_code_products_id` int(11) NOT NULL,
  `products_inventory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pcp_inventory`
--

INSERT INTO `pcp_inventory` (`id`, `product_code_products_id`, `products_inventory_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 6, 9),
(10, 7, 10),
(11, 6, 11),
(12, 7, 12);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `description`, `added_on`) VALUES
(1, 'OWNER', '2021-09-25 22:29:45'),
(2, 'CLIENT', '2021-09-26 14:24:39'),
(3, 'STAFF', '2021-09-26 14:24:39');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_refnum` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `unit` varchar(250) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_refnum`, `name`, `description`, `unit`, `status`, `added_on`, `added_by`, `updated_on`, `updated_by`) VALUES
(1, 'NE-00001', 'T-Shirt [S]', 'T-Shirt size Small', 'pcs', 'ACTIVE', '2021-09-26 00:32:40', 1, '2021-10-04 21:23:11', 1),
(2, 'NE-00002', 'T-Shirt [M]', 'T-Shirt size Medium', 'pcs', 'ACTIVE', '2021-09-26 00:33:07', 1, NULL, 0),
(3, 'NE-00003', 'T-Shirt [L]', 'T-Shirt size Large', 'pcs', 'ACTIVE', '2021-09-26 00:33:07', 1, NULL, 0),
(6, 'NE-00004', 'T-Shirt [XL]', 'T-Shirt size Extra-Large', 'pcs', 'ACTIVE', '2021-10-03 18:26:56', 1, '2021-10-03 22:03:22', 1),
(7, 'WP-00001', 'Black Syrup', 'Black Syrup 1 Liter', 'liters', 'ACTIVE', '2021-10-06 22:36:26', 1, '2021-10-07 10:08:32', 1),
(8, 'DP-00001', 'Assam Black Tea', 'Assam Black Tea 80g', 'grams', 'ACTIVE', '2021-10-06 22:46:53', 1, NULL, NULL),
(9, 'DP-00002', 'Blue Tea', 'Blue Tea 8g', 'grams', 'ACTIVE', '2021-10-06 22:53:49', 1, NULL, NULL),
(10, 'WP-00002', 'Brown Syrup', 'Brown Syrup 1 Liter', 'liters', 'ACTIVE', '2021-10-06 22:58:56', 1, '2021-10-07 10:08:46', 1),
(11, 'WP-00003', 'Fructose', 'Fructose 1 Liter', 'liters', 'ACTIVE', '2021-10-07 10:05:51', 1, '2021-10-07 10:08:58', 1),
(12, 'WP-00004', 'Fresh Milk', 'Fresh Milk 1 Liter', 'liters', 'ACTIVE', '2021-10-07 10:07:25', 1, '2021-10-07 10:09:08', 1),
(13, 'WP-00005', 'Whipping Cream', 'Whipping Cream 1Liter', 'liters', 'ACTIVE', '2021-10-07 10:08:11', 1, NULL, NULL),
(14, 'DP-00003', 'Black Tapioca', 'Black Tapioca 1 kilogram', 'kg', 'ACTIVE', '2021-10-07 10:22:39', 1, NULL, NULL),
(15, 'DP-00004', 'Mini Tapioca', 'Mini Tapioca 1 kilogram', 'kg', 'ACTIVE', '2021-10-07 10:28:16', 1, NULL, NULL),
(16, 'WP-00006', 'Nata de Coco', 'Nata de Coco 2.5 kilograms', 'kg', 'ACTIVE', '2021-10-07 10:29:20', 1, '2021-10-07 10:33:50', 1),
(17, 'WP-00007', 'Popping Mango', 'Popping Mango 3.2 kilograms', 'kg', 'ACTIVE', '2021-10-07 10:33:34', 1, NULL, NULL),
(18, 'WP-00008', 'Popping Strawberry', 'Popping Strawberry 3.2 kilograms', 'kg', 'ACTIVE', '2021-10-07 10:34:55', 1, NULL, NULL),
(19, 'WP-00009', 'Popping Lychee', 'Popping Lychee 3.2 kilograms', 'kg', 'ACTIVE', '2021-10-07 10:35:39', 1, NULL, NULL),
(20, 'DP-00005', 'Sugar Bits [R]', 'Sugar Bits Regular', 'pack', 'ACTIVE', '2021-10-07 10:40:23', 1, NULL, NULL),
(21, 'DP-00006', 'Sugar Bits [S]', 'Sugar Bits Small', 'pack', 'ACTIVE', '2021-10-07 10:41:07', 1, NULL, NULL),
(22, 'DP-00007', 'Cream Cheese', 'Cream Cheese per pack', 'pack', 'ACTIVE', '2021-10-07 10:43:50', 1, NULL, NULL),
(23, 'NE-00005', 'Plastic Gloves', 'Plastic Gloves', 'pack', 'ACTIVE', '2021-10-08 13:49:02', 1, NULL, NULL),
(24, 'NE-00006', 'Visor', 'Visor', 'pcs', 'ACTIVE', '2021-10-08 13:51:10', 1, NULL, NULL),
(25, 'NE-00007', 'Nitrile Gloves', 'Nitrile Gloves Disposable', 'pcs', 'ACTIVE', '2021-10-08 13:52:35', 1, NULL, NULL),
(26, 'NE-00008', 'Spit Guard', 'Spit Guard', 'pcs', 'ACTIVE', '2021-10-08 13:53:24', 1, NULL, NULL),
(27, 'NE-00009', 'Apron', 'Apron', 'pcs', 'ACTIVE', '2021-10-08 13:54:10', 1, NULL, NULL),
(28, 'NE-00010', 'Hairnet', 'Hairnet', 'pcs', 'ACTIVE', '2021-10-08 13:54:45', 1, NULL, NULL),
(29, 'NE-00011', 'Tea Net', 'Tea Net', 'pcs', 'ACTIVE', '2021-10-08 13:56:55', 1, NULL, NULL),
(30, 'NE-00012', 'Double Take out bag', 'Double Take out bag 100pcs/pack', 'pack', 'ACTIVE', '2021-10-08 13:57:40', 1, NULL, NULL),
(31, 'NE-00013', 'Single Take out bag', 'Single Take out bag 100pcs/pack', 'pack', 'ACTIVE', '2021-10-08 13:58:50', 1, NULL, NULL),
(32, 'NE-00014', 'Black Straw', 'Black Straw 100pcs/pack', 'pack', 'ACTIVE', '2021-10-08 13:59:26', 1, '2021-10-08 14:03:23', 1),
(33, 'NE-00015', 'White Straw', 'White Straw 100pcs/pack', 'pack', 'ACTIVE', '2021-10-08 13:59:57', 1, '2021-10-08 14:03:08', 1),
(34, 'NE-00016', 'Transparent Lids', 'Transparent Lids 50pcs/pack', 'pack', 'ACTIVE', '2021-10-08 14:00:31', 1, '2021-10-08 14:02:34', 1),
(35, 'NE-00017', 'Grande Cup', 'Grande Cup 40pcs/pack', 'pack', 'ACTIVE', '2021-10-08 14:01:25', 1, '2021-10-08 14:02:51', 1),
(36, 'NE-00018', 'Medio Cup', 'Medio Cup 25pcs/pack', 'pack', 'ACTIVE', '2021-10-08 14:01:58', 1, NULL, NULL),
(37, 'NE-00019', 'Piccolo Cup', 'Piccolo Cup 25pcs/pack', 'pack', 'ACTIVE', '2021-10-08 14:10:52', 1, NULL, NULL),
(38, 'WP-00010', 'Pepper Dip', 'Pepper Dip 280grams', 'grams', 'ACTIVE', '2021-10-08 14:11:35', 1, NULL, NULL),
(39, 'WP-00011', 'Cheese Dip', 'Cheese Dip 280grams', 'grams', 'ACTIVE', '2021-10-08 14:12:01', 1, NULL, NULL),
(40, 'WP-00012', 'Nachos Sauce', 'Nachos Sauce 700grams', 'grams', 'ACTIVE', '2021-10-08 14:15:19', 1, NULL, NULL),
(41, 'DP-00008', 'Nachos', 'Nachos 50grans', 'grams', 'ACTIVE', '2021-10-08 14:17:24', 1, NULL, NULL),
(42, 'WP-00013', 'Chicken Wings', 'Chicken Wings 3kilograms', 'kg', 'ACTIVE', '2021-10-08 14:24:19', 1, NULL, NULL),
(43, 'WP-00014', 'Korean Wings Sauce', 'Korean Wings Sauce 900g', 'grams', 'ACTIVE', '2021-10-08 14:25:55', 1, NULL, NULL),
(44, 'WP-00015', 'Garlic Parmesan Wings Sauce', 'Garlic Parmesan Wings Sauce 800g', 'grams', 'ACTIVE', '2021-10-08 14:27:20', 1, NULL, NULL),
(45, 'WP-00016', 'Honey BBQ Wings Sauce', 'Honey BBQ Wings Sauce 900g', 'grams', 'ACTIVE', '2021-10-08 14:27:48', 1, NULL, NULL),
(46, 'WP-00017', 'Mongolian Wings Sauce', 'Mongolian Wings Sauce 1000g', 'grams', 'ACTIVE', '2021-10-08 14:28:35', 1, NULL, NULL),
(47, 'WP-00018', 'Lemon Pepper Wings Sauce', 'Lemon Pepper Wings Sauce 750g', 'grams', 'ACTIVE', '2021-10-08 14:29:16', 1, NULL, NULL),
(48, 'WP-00019', 'Buffalo Wings Sauce', 'Buffalo Wings Sauce 900g', 'grams', 'ACTIVE', '2021-10-08 14:29:46', 1, NULL, NULL),
(49, 'DP-00009', 'Skin-on Fries', 'Skin-on Fries 1 kilogram', 'kg', 'ACTIVE', '2021-10-08 14:31:18', 1, NULL, NULL),
(50, 'WP-00020', 'Lemon', 'Lemon 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:33:17', 1, NULL, NULL),
(51, 'WP-00021', 'Mango', 'Mango 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:34:13', 1, NULL, NULL),
(52, 'WP-00022', 'Passion Fruit 2.5L', 'Passion Fruit 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:34:47', 1, NULL, NULL),
(53, 'WP-00023', 'Honey 2.5L', 'Honey 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:35:19', 1, NULL, NULL),
(54, 'WP-00024', 'Peach', 'Peach 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:35:41', 1, NULL, NULL),
(55, 'WP-00025', 'Kiwi', 'Kiwi 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:36:04', 1, NULL, NULL),
(56, 'WP-00026', 'Strawberry', 'Strawberry 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:36:25', 1, NULL, NULL),
(57, 'WP-00027', 'Lychee', 'Lychee 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:36:52', 1, NULL, NULL),
(58, 'WP-00028', 'Green Apple', 'Green Apple 2.5Liters', 'liters', 'ACTIVE', '2021-10-08 14:37:20', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_inventory`
--

CREATE TABLE `products_inventory` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_code_products_id` int(11) NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `starting_qty` int(11) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `remarks` varchar(250) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_inventory`
--

INSERT INTO `products_inventory` (`id`, `branch_id`, `product_code_products_id`, `expiration_date`, `starting_qty`, `status`, `remarks`, `added_on`, `added_by`) VALUES
(1, 1, 1, NULL, 5, 'ACTIVE', 'New Stock', '2021-09-26 00:36:09', 0),
(2, 1, 2, NULL, 5, 'ACTIVE', 'New Stock', '2021-09-26 00:36:23', 0),
(3, 1, 3, NULL, 5, 'ACTIVE', 'New Stock', '2021-09-26 00:36:23', 0),
(4, 1, 1, NULL, 5, 'ACTIVE', 'New Stock', '2021-09-26 01:09:02', 0),
(8, 1, 1, NULL, 3, 'ACTIVE', 'New Stocks', '2021-10-10 23:42:46', 1),
(9, 1, 6, '2021-10-11', 5, 'ACTIVE', 'Sample', '2021-10-11 17:59:31', 1),
(10, 1, 7, '2023-03-02', 5, 'ACTIVE', '5', '2021-10-17 16:26:37', 1),
(11, 1, 6, '2022-03-02', 10, 'ACTIVE', 'New Stock', '2021-10-20 08:56:50', 3),
(12, 1, 7, '2021-10-30', 2, 'ACTIVE', 'New Stocks', '2021-10-24 18:41:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_code`
--

CREATE TABLE `product_code` (
  `id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_code`
--

INSERT INTO `product_code` (`id`, `code`, `description`, `status`, `added_on`, `added_by`) VALUES
(1, 'NE', 'NON-EDIBLE', 'ACTIVE', '2021-09-26 00:31:28', 1),
(2, 'DP', 'DRY PRODUCTS', 'ACTIVE', '2021-10-03 16:07:02', 1),
(3, 'WP', 'WET PRODUCTS', 'ACTIVE', '2021-10-06 11:05:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_code_products`
--

CREATE TABLE `product_code_products` (
  `id` int(11) NOT NULL,
  `product_code_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_code_products`
--

INSERT INTO `product_code_products` (`id`, `product_code_id`, `products_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 6),
(5, 3, 7),
(6, 2, 8),
(7, 2, 9),
(8, 3, 10),
(9, 3, 11),
(10, 3, 12),
(11, 3, 13),
(12, 2, 14),
(13, 2, 15),
(14, 3, 16),
(15, 3, 17),
(16, 3, 18),
(17, 3, 19),
(18, 2, 20),
(19, 2, 21),
(20, 2, 22),
(21, 1, 23),
(22, 1, 24),
(23, 1, 25),
(24, 1, 26),
(25, 1, 27),
(26, 1, 28),
(27, 1, 29),
(28, 1, 30),
(29, 1, 31),
(30, 1, 32),
(31, 1, 33),
(32, 1, 34),
(33, 1, 35),
(34, 1, 36),
(35, 1, 37),
(36, 3, 38),
(37, 3, 39),
(38, 3, 40),
(39, 2, 41),
(40, 3, 42),
(41, 3, 43),
(42, 3, 44),
(43, 3, 45),
(44, 3, 46),
(45, 3, 47),
(46, 3, 48),
(47, 2, 49),
(48, 3, 50),
(49, 3, 51),
(50, 3, 52),
(51, 3, 53),
(52, 3, 54),
(53, 3, 55),
(54, 3, 56),
(55, 3, 57),
(56, 3, 58);

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `id` int(11) NOT NULL,
  `product_code_products_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`id`, `product_code_products_id`, `price`, `status`, `added_on`) VALUES
(1, 1, 450, 'ACTIVE', '2021-09-26 00:34:26'),
(2, 2, 450, 'ACTIVE', '2021-09-26 00:34:37'),
(3, 3, 450, 'ACTIVE', '2021-09-26 00:34:37'),
(4, 4, 450, 'INACTIVE', '2021-10-03 18:26:56'),
(5, 4, 451, 'INACTIVE', '2021-10-03 22:01:04'),
(6, 4, 450, 'ACTIVE', '2021-10-03 22:01:20'),
(7, 5, 100, 'INACTIVE', '2021-10-06 22:36:26'),
(8, 6, 46, 'ACTIVE', '2021-10-06 22:46:53'),
(9, 5, 180, 'ACTIVE', '2021-10-06 22:48:04'),
(10, 7, 34, 'ACTIVE', '2021-10-06 22:53:49'),
(11, 8, 100, 'ACTIVE', '2021-10-06 22:58:56'),
(12, 9, 110, 'ACTIVE', '2021-10-07 10:05:51'),
(13, 10, 90, 'ACTIVE', '2021-10-07 10:07:25'),
(14, 11, 400, 'ACTIVE', '2021-10-07 10:08:11'),
(15, 12, 100, 'ACTIVE', '2021-10-07 10:22:39'),
(16, 13, 110, 'ACTIVE', '2021-10-07 10:28:16'),
(17, 14, 400, 'ACTIVE', '2021-10-07 10:29:20'),
(18, 15, 700, 'ACTIVE', '2021-10-07 10:33:34'),
(19, 16, 700, 'ACTIVE', '2021-10-07 10:34:55'),
(20, 17, 700, 'ACTIVE', '2021-10-07 10:35:39'),
(21, 18, 30, 'ACTIVE', '2021-10-07 10:40:23'),
(22, 19, 20, 'ACTIVE', '2021-10-07 10:41:07'),
(23, 20, 120, 'ACTIVE', '2021-10-07 10:43:50'),
(24, 21, 60, 'ACTIVE', '2021-10-08 13:49:02'),
(25, 22, 250, 'ACTIVE', '2021-10-08 13:51:10'),
(26, 23, 30, 'ACTIVE', '2021-10-08 13:52:36'),
(27, 24, 30, 'ACTIVE', '2021-10-08 13:53:24'),
(28, 25, 180, 'ACTIVE', '2021-10-08 13:54:10'),
(29, 26, 15, 'ACTIVE', '2021-10-08 13:54:45'),
(30, 27, 100, 'ACTIVE', '2021-10-08 13:56:55'),
(31, 28, 120, 'ACTIVE', '2021-10-08 13:57:40'),
(32, 29, 100, 'ACTIVE', '2021-10-08 13:58:50'),
(33, 30, 100, 'ACTIVE', '2021-10-08 13:59:26'),
(34, 31, 100, 'ACTIVE', '2021-10-08 13:59:57'),
(35, 32, 125, 'ACTIVE', '2021-10-08 14:00:31'),
(36, 33, 244, 'ACTIVE', '2021-10-08 14:01:26'),
(37, 34, 142.5, 'ACTIVE', '2021-10-08 14:01:58'),
(38, 35, 130, 'ACTIVE', '2021-10-08 14:10:52'),
(39, 36, 140, 'ACTIVE', '2021-10-08 14:11:35'),
(40, 37, 60, 'ACTIVE', '2021-10-08 14:12:01'),
(41, 38, 211, 'ACTIVE', '2021-10-08 14:15:19'),
(42, 39, 5, 'ACTIVE', '2021-10-08 14:17:24'),
(43, 40, 555, 'ACTIVE', '2021-10-08 14:24:19'),
(44, 41, 280, 'ACTIVE', '2021-10-08 14:25:55'),
(45, 42, 325, 'ACTIVE', '2021-10-08 14:27:20'),
(46, 43, 380, 'ACTIVE', '2021-10-08 14:27:48'),
(47, 44, 325, 'ACTIVE', '2021-10-08 14:28:35'),
(48, 45, 590, 'ACTIVE', '2021-10-08 14:29:16'),
(49, 46, 395, 'ACTIVE', '2021-10-08 14:29:46'),
(50, 47, 176, 'ACTIVE', '2021-10-08 14:31:18'),
(51, 48, 481, 'ACTIVE', '2021-10-08 14:33:17'),
(52, 49, 481, 'ACTIVE', '2021-10-08 14:34:13'),
(53, 50, 481, 'ACTIVE', '2021-10-08 14:34:48'),
(54, 51, 540, 'ACTIVE', '2021-10-08 14:35:19'),
(55, 52, 516, 'ACTIVE', '2021-10-08 14:35:41'),
(56, 53, 516, 'ACTIVE', '2021-10-08 14:36:04'),
(57, 54, 481, 'ACTIVE', '2021-10-08 14:36:25'),
(58, 55, 481, 'ACTIVE', '2021-10-08 14:36:52'),
(59, 56, 518, 'ACTIVE', '2021-10-08 14:37:20');

-- --------------------------------------------------------

--
-- Table structure for table `returned_items`
--

CREATE TABLE `returned_items` (
  `id` int(11) NOT NULL,
  `order_list_id` int(11) NOT NULL,
  `products_inventory_id` int(11) NOT NULL,
  `return_code_number` varchar(500) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_price` float NOT NULL,
  `date_added` datetime NOT NULL DEFAULT current_timestamp(),
  `transacted_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `returned_items`
--

INSERT INTO `returned_items` (`id`, `order_list_id`, `products_inventory_id`, `return_code_number`, `qty`, `total_price`, `date_added`, `transacted_by`, `updated_by`, `updated_on`) VALUES
(3, 3, 9, 'RET#0000000001', 1, 46, '2021-10-23 20:23:51', 1, 1, '2021-10-25 20:08:44'),
(4, 4, 10, 'RET#0000000004', 1, 34, '2021-10-25 20:08:44', 1, NULL, NULL),
(5, 7, 9, 'RET#0000000005', 1, 46, '2021-10-28 06:12:06', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shop_cart`
--

CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  `product_code_products_id` int(11) NOT NULL,
  `cart_order_qty` int(11) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `order_tracking_no` varchar(500) DEFAULT NULL,
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shop_cart`
--

INSERT INTO `shop_cart` (`id`, `user_info_id`, `product_code_products_id`, `cart_order_qty`, `status`, `order_tracking_no`, `added_on`, `updated_on`) VALUES
(1, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000001', '2021-10-17 00:49:27', '2021-10-17 23:00:29'),
(2, 5, 7, 2, 'CHECKED-OUT', 'PO#0000000001', '2021-10-17 16:27:05', '2021-10-17 23:00:29'),
(3, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000007', '2021-10-19 22:27:22', '2021-10-19 22:27:27'),
(4, 5, 6, 2, 'CHECKED-OUT', 'PO#0000000008', '2021-10-20 21:50:49', '2021-10-20 21:50:57'),
(5, 5, 6, 2, 'CHECKED-OUT', 'PO#0000000009', '2021-10-24 00:51:19', '2021-10-24 00:51:38'),
(6, 5, 6, 2, 'CHECKED-OUT', 'PO#0000000010', '2021-10-24 02:14:23', '2021-10-24 02:14:27'),
(7, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000011', '2021-10-27 20:32:05', '2021-10-27 20:32:11'),
(8, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000012', '2021-10-27 20:32:39', '2021-10-27 20:33:07'),
(9, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000013', '2021-10-27 20:34:44', '2021-10-27 20:34:51'),
(10, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000014', '2021-10-27 20:36:17', '2021-10-27 20:36:46'),
(11, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000015', '2021-10-27 20:39:05', '2021-10-27 20:39:14'),
(12, 5, 6, 1, 'CHECKED-OUT', 'PO#0000000016', '2021-10-28 06:20:31', '2021-10-28 06:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `stakeholders`
--

CREATE TABLE `stakeholders` (
  `id` int(11) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `midname` varchar(250) DEFAULT NULL,
  `lastname` varchar(250) NOT NULL,
  `extname` varchar(250) DEFAULT NULL,
  `contact_num` bigint(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `added_on` datetime NOT NULL DEFAULT current_timestamp(),
  `added_by` int(11) NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stakeholders`
--

INSERT INTO `stakeholders` (`id`, `firstname`, `midname`, `lastname`, `extname`, `contact_num`, `image`, `status`, `added_on`, `added_by`, `updated_on`, `updated_by`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 9999999999, 'public/assets/content/images/temp/person-icon.png', 'ACTIVE', '2021-09-25 00:00:00', 1, NULL, 0),
(4, 'Staff', 'no', '1', '', 9999999997, 'public/assets/content/images/temp/person-icon.png', 'ACTIVE', '2021-09-26 15:54:12', 1, '2021-10-03 22:36:38', 1),
(5, 'Staff', '', '2', '', 9999999999, 'public/assets/content/images/temp/person-icon.png', 'ACTIVE', '2021-09-26 16:14:51', 1, '2021-10-10 01:13:51', 1),
(6, 'Owner', '', '1', '', 99999999999, 'public/assets/content/images/temp/person-icon.png', 'ACTIVE', '2021-10-03 22:54:15', 1, NULL, 0),
(8, 'Owner', '', '2', '', 99999999999, 'public/assets/content/images/temp/person-icon.png', 'ACTIVE', '2021-10-03 23:00:05', 1, '2021-10-11 22:14:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_credentials`
--

CREATE TABLE `user_credentials` (
  `id` int(11) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE',
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_credentials`
--

INSERT INTO `user_credentials` (`id`, `user_info_id`, `user_role_id`, `username`, `password`, `status`, `added_on`) VALUES
(1, 1, 1, 'admin', '0192023a7bbd73250516f069df18b500', 'ACTIVE', '2021-09-25 22:32:13'),
(2, 3, 2, 'staff.1', '015a79291e177a87d53783f13a3c74b9', 'ACTIVE', '2021-09-26 15:54:12'),
(3, 4, 2, 'staff.2', 'c47bae658647828873ba6eb51118f284', 'ACTIVE', '2021-09-26 16:14:51'),
(4, 5, 3, 'owner.1', '936253beb5513ac5f249e6de3f8fe91b', 'ACTIVE', '2021-10-03 22:54:15'),
(6, 7, 3, 'owner.2', 'd86ada448fba31b631d96e48aeea0fc9', 'ACTIVE', '2021-10-03 23:00:05');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `stakeholders_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `stakeholders_id`, `position_id`, `branch_id`) VALUES
(1, 1, 1, 1),
(3, 4, 3, 1),
(4, 5, 3, 1),
(5, 6, 2, 2),
(7, 8, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `description`, `added_on`) VALUES
(1, 'SUPERADMIN', '2021-09-25 22:30:58'),
(2, 'ADMIN', '2021-09-26 15:02:09'),
(3, 'CLIENT', '2021-10-03 22:48:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_info_id` (`user_info_id`);

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`id`),
  ADD KEY `viewer_id` (`viewer_id`);

--
-- Indexes for table `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_price_id` (`product_price_id`);

--
-- Indexes for table `order_list_inventory`
--
ALTER TABLE `order_list_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_list_id` (`order_list_id`),
  ADD KEY `products_inventory_id` (`products_inventory_id`),
  ADD KEY `transacted_by` (`transacted_by`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `processed_by` (`processed_by`);

--
-- Indexes for table `order_tbl`
--
ALTER TABLE `order_tbl`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `pcp_inventory`
--
ALTER TABLE `pcp_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_code-products_id` (`product_code_products_id`),
  ADD KEY `products_inventory_id` (`products_inventory_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_inventory`
--
ALTER TABLE `products_inventory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `product_code-products_id` (`product_code_products_id`);

--
-- Indexes for table `product_code`
--
ALTER TABLE `product_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_code_products`
--
ALTER TABLE `product_code_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_code_id` (`product_code_id`),
  ADD KEY `products_id` (`products_id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_code_products_id` (`product_code_products_id`);

--
-- Indexes for table `returned_items`
--
ALTER TABLE `returned_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_list_id` (`order_list_id`),
  ADD KEY `product_code_products_id` (`products_inventory_id`);

--
-- Indexes for table `shop_cart`
--
ALTER TABLE `shop_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_info_id` (`user_info_id`),
  ADD KEY `product_price_id` (`product_code_products_id`),
  ADD KEY `product_code_products_id` (`product_code_products_id`);

--
-- Indexes for table `stakeholders`
--
ALTER TABLE `stakeholders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_credentials`
--
ALTER TABLE `user_credentials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_info_id` (`user_info_id`),
  ADD KEY `user_role_id` (`user_role_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stakeholders_id` (`stakeholders_id`),
  ADD KEY `position_id` (`position_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `order_list`
--
ALTER TABLE `order_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `order_list_inventory`
--
ALTER TABLE `order_list_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `order_tbl`
--
ALTER TABLE `order_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pcp_inventory`
--
ALTER TABLE `pcp_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `products_inventory`
--
ALTER TABLE `products_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product_code`
--
ALTER TABLE `product_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_code_products`
--
ALTER TABLE `product_code_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `product_price`
--
ALTER TABLE `product_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `returned_items`
--
ALTER TABLE `returned_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shop_cart`
--
ALTER TABLE `shop_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `stakeholders`
--
ALTER TABLE `stakeholders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_credentials`
--
ALTER TABLE `user_credentials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
