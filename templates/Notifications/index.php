<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<style>
    /* Pagination links */
    .pagination {
        padding-top: 5px;
    }
    .pagination a {
        color: #db0c34;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination li.active a {
        background-color: #db0c34;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}
</style>
    <?= $this->Form->create()?><?= $this->Form->end()?>
    <aside class="left-sidebar">
        <div style="text-align:center; height: 12% !important; background-color: #ffffff;">
            <?= $this->Html->image('YNS_logo_2.jpg', ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:20%;']) ?>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && ($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Posts') ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-home']) . 'HOME', '/home', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && (($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Users') || ($this->request->getAttribute('params')['action'] === 'following' && $this->request->getAttribute('params')['controller'] === 'FollowersFollowing')  || ($this->request->getAttribute('params')['action'] === 'followers' && $this->request->getAttribute('params')['controller'] === 'FollowersFollowing')) ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-account']) . 'PROFILE', '/profile', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && ($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Notifications') ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-bell']) . 'NOTIFICATIONS', '/notifications', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
            </ul>
        </nav>

        <div class="sidebar-footer" style="padding-bottom: 20px;">
            <div class="row" style="margin: 0px !important;padding: 0px !important; height: 100%;">
                <div style="padding-left: 20px; padding-top:20px;">
                    <?= $this->Html->image('profile_pics/' . h($user['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:60px; height:60px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                </div>
                <div style="padding-left: 20px; padding-top: 25px;">
                    <b><?= h($user['nickname']) === null || h($user['nickname']) === '' ? h($user['firstname']) . ' ' . h($user['lastname']) : h($user['nickname'])?></b>
                    <br/>
                    <?= '@' . h($user['username'])?>
                </div>
                <div style="padding-left: 15px; padding-top: 17px;">
                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-logout', 'style' => 'font-size: 25px;']), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?>
                </div>
            </div>
            
        </div>
    </aside>
    <div class="page-wrapper" style="padding: 0px !important; height:100%; !important;">
        <div class="container-fluid" style="margin: 0px !important;padding: 0px !important; height:100%; !important;">
            <div class="row" style="margin: 0px !important;padding: 0px !important; height: 100% !important;">
                <div class="col-md-8" style="margin: 0px !important;padding: 0px !important; height: 100% !important; background-color: #ffffff0; border-right: #ebebe0 1px solid; border-left: #ebebe0 1px solid;">
                    <div style="height: 10.73% !important; padding-top: 12px; padding-left: 25px; border-bottom: #ebebe0 1px solid;">
                        <div style="height:40%">
                            <b style="font-size:20px;"><?= h($user['nickname']) === null || h($user['nickname']) === '' ? h($user['firstname']) . ' ' . h($user['lastname']) : h($user['nickname'])?></b>
                        </div>
                        <div style="height:50%">
                            <small style="font-size:13px;"><?= $notifications->count() ?> Notifications</small>
                        </div>
                    </div>
                    <div style="height: 7% !important; padding-top: 15px; padding-left: 25px;">
                        <h5>NOTIFICATIONS</h5>
                    </div>
                    <div style="padding-top: 7px; border-bottom: #ebebe0 1px solid;"></div>
                    <div style="height: 464px;">
                    <?php foreach ($notifications as $notif) {?>
                        <div class="row" style="height:80px; margin: 0px !important; padding-left: 25px; padding-right: 25px; padding-top:20px; padding-bottom:10px; border-bottom: #ebebe0 1px solid;">
                            <div style="padding-left: 5px;">
                                <?= $this->Html->image('profile_pics/' . h($notif['user']['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                            </div>
                            <div style="padding-left: 10px; padding-top: 0px; font-size: 12px;">
                                <?php $idParam = base64_encode(base64_encode(base64_encode(h($notif['user']['id']))))?>
                                <b><?= $this->Html->link( h($notif['user']['nickname']) === null || h($notif['user']['nickname']) === '' ? h($notif['user']['firstname']) . ' ' . h($notif['user']['lastname']) : h($notif['user']['nickname']),  ['controller' => 'Users', 'action' => 'user', $idParam]) ?></b>
                                (<?= '@' . h($notif['user']['username'])?>) · <?= date('Y', strtotime(h($notif['date_created']))) === date('Y') ? date('M d', strtotime(h($notif['date_created']))) : date('m/d/Y', strtotime(h($notif['date_created'])))?>
                                <br/> <?= !empty($notif['post']) ? substr(h($notif['content']), 0, 20) . ' "' . $this->Html->link((strlen(h($notif['post']['content'])) > 20 ? substr(h($notif['post']['content']), 0, 20) . '...' :  h($notif['post']['content'])),  ['controller' => 'Comments', 'index' => 'user', base64_encode(base64_encode(base64_encode(h($notif['post']['id']))))]) . '"' : substr(h($notif['content']), 0, 20)?>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="paginator">
                        <ul class="pagination"  style="padding-bottom: 0px !important; margin: 0px !important;">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers(array('modulus' => 4)) ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <small style="padding-left: 15px;"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></small>
                    </div>
                </div>
                <div class="col-md-4" style="margin: 0px !important;padding: 0px !important;height: 100% !important;">
                    <div style="text-align:center; height: 9.4% !important; padding-top: 14px; position: relative">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'search'], 'type' => 'get'])?>
                        <?= $this->Form->control('key', ['label' => false, 'class' => 'form-control', 'placeholder' => 'search...', 'style' => 'width: 80%; display: inline-block; padding-top: 3px;', 'value' => $this->request->getQuery('key')])?>
                        <?= $this->Form->end()?>
                    </div>
                    <div style="padding-top:7px;border-bottom: #ebebe0 1px solid;"></div>
                    <div style="height: 7% !important; padding-top: 15px; padding-left: 25px;">
                        <h5>WHO TO FOLLOW</h5>
                    </div>
                    <div style="padding-top: 7px; border-bottom: #ebebe0 1px solid"></div>
                    <?php foreach ($users as $toFollow) {?>
                        <div class="row" style="height:80px; margin: 0px !important; padding-left: 25px; padding-right: 25px; padding-top:20px; padding-bottom:10px; border-bottom: #ebebe0 1px solid;">
                            <div style="padding-left: 5px;">
                                <?= $this->Html->image('profile_pics/' . h($toFollow['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                            </div>
                            <div style="padding-left: 10px; padding-top: 0px; font-size: 12px;">
                                <?php $idParam = base64_encode(base64_encode(base64_encode(h($toFollow['id']))))?>
                                <b><?= $this->Html->link( h($toFollow['nickname']) === null || h($toFollow['nickname']) === '' ? h($toFollow['firstname']) . ' ' . h($toFollow['lastname']) : h($toFollow['nickname']), ['controller' => 'Users', 'action' => 'user', $idParam]) ?></b>
                                <br/>(<?= '@' . h($toFollow['username'])?>)
                            </div>
                            <div style="padding-left: 50px; padding-top: 3px;">
                                <button class="btn btn-sm btn-yns" onClick="follow(<?= h($toFollow['id'])?>)">Follow</button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!---content---->

