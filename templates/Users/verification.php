<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<style>
    .div_spacing-10{
        padding-top:10px;
    }
</style>

<div class="row justify-content-center" style="margin: 0px !important;padding: 0px !important;">
    <div class="col-lg-12 col-md-12" style="margin:auto; padding-top:200px; padding-left:50px; padding-right:50px;">
        <?php  if (!empty($user) && isset($user['id'])) { ?>
            
                <h3> Your account has been successfully verified! You may now log-in using your registered credentials. 
                    Please click on the <b>'Go To Login Page'</b> button to be redirected to the login page. </h3>
                
                <div style="padding-top: 50px;"></div>

                <?= $this->Html->link(__('Go To Login Page'), '/', ['class' => 'btn btn-lg btn-yns', 'style' => 'text-align: center;']) ?>
            
        <?php } else { ?>
            <h3> An error has occured. Please try again later or call for a support. </h3>
        <?php } ?>
    </div>
</div>