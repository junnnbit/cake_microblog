<style>
    .div_spacing-10{
        padding-top:10px;
    }
</style>

<div class="row" style="margin: 0px !important;padding: 0px !important;">
  <div class="col-md-6" style="margin: 0px !important;padding: 0px !important;">
    <?= $this->Html->image('bg_yns.JPG', ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:100%;object-fit: cover;']) ?>
  </div>
  <div class="col-md-6 contents" style="margin: 0px !important;padding: 0px !important;">
    <div class="row justify-content-center" style="margin: 0px !important;padding: 0px !important;">
      <div class="col-md-7">
        <div class="mb-4" style="padding-top: 50px;">
          <h3 style="text-align: center; color: #db0c34;">Social life at YNS!</h3>
        </div>
        <?= $this->Form->create() ?>
        <div style="border: 1px solid black; padding: 50px !important;">
          <div class="form-group first">
            <label for="username">Username</label>
            <?= $this->Form->control('username', ['class' => 'form-control', 'label' => false, 'required' => true])?>
          </div>
          <div class="form-group last mb-4">
            <label for="password">Password</label>
            <?= $this->Form->control('password', ['class' => 'form-control', 'type' => 'password', 'label' => false, 'required' => true])?>
          </div>
              
          <div class="mb-4">
            <!-- <span class="d-block text-center"><a href="#" class="forgot-pass">Forgot Password?</a></span>  -->
          </div>

          <?= $this->Form->button(__('LOG IN'), ['class' => 'btn btn-block btn-yns']) ?>

          <span class="d-block text-center my-4 text-muted">&mdash;&mdash;&mdash;&mdash;&mdash; OR &mdash;&mdash;&mdash;&mdash;&mdash;</span>
              
          <button type="button" class="btn btn-block btn-yns" onclick="openModal()">SIGN-UP!</button>
        
        </div>  
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signUpModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="signUpModalLabel">Register Now!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create()?>
      <div class="modal-body signUpModalBody">
        <div class="row formInputs">
          <!-- First Name -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="firstname"><small style="color:red;">* </small> First Name</label>
          </div>
          <div class="col-md-7">
            <input type="text" class="form-control reg_firstname required" id="firstname" name="firstname">
          </div>
          <!-- End of First Name -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Middle Name -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="middlename"> Middle Name</label>
          </div>
          <div class="col-md-7">
            <input type="text" class="form-control reg_middlename" id="middlename" name="middlename">
          </div>
          <!-- End of Middle Name -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Last Name -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="lastname"><small style="color:red;">* </small> Last Name</label>
          </div>
          <div class="col-md-7">
            <input type="text" class="form-control reg_lastname required" id="lastname" name="lastname">
          </div>
          <!-- End of Last Name -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Birthdate -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="birthdate"><small style="color:red;">* </small> Birthdate</label>
          </div>
          <div class="col-md-7">
            <input type="date" class="form-control reg_birthdate required" id="birthdate" name="birthdate">
          </div>
          <!-- End of Birthdate -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Gender -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="gender"><small style="color:red;">* </small> Gender</label>
          </div>
          <div class="col-md-7">
            <select class="form-control reg_gender required" id="gender" name="gender">
              <option value="" selected disabled>--- Please select ---</option>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div>
          <!-- End of Gender -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Email -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="email"><small style="color:red;">* </small> Email</label>
          </div>
          <div class="col-md-7">
            <input type="text" class="form-control reg_email required" id="email" name="email">
          </div>
          <!-- End of Email -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Username -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="username"><small style="color:red;">* </small> Username</label>
          </div>
          <div class="col-md-7">
            <input type="text" class="form-control reg_username required" id="username" name="username">
          </div>
          <!-- End of Username -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Password -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="password"><small style="color:red;">* </small> Password</label>
          </div>
          <div class="col-md-7">
            <input type="password" class="form-control reg_password required" id="password" name="password">
          </div>
          <!-- End of Password -->

          <div class="col-md-12 div_spacing-10"></div>

          <!-- Confirm Password -->
          <div class="col-md-5" style="padding-top:10px;">
            <label for="conf_password"><small style="color:red;">* </small> Confirm Password</label>
          </div>
          <div class="col-md-7">
            <input type="password" class="form-control reg_confpass required" id="conf_password" name="conf_password">
          </div>
          <!-- End of Confirm Password -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?= $this->Form->button(__('SIGN-UP!'), ['type' => 'button', 'class' => 'btn btn-yns submit_signup'])?>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>

<script>
  function openModal() {
    $('#signupModal').modal('show');
  }

  $('#signupModal').on('hidden.bs.modal', function() {
    $('.formInputs').find('input').each(function() {
      $(this).val('');
    });
    $('.formInputs').find('select').each(function() {
      $(this).val('');
    });
    $(".signup_req").remove();
  });

  $('.submit_signup').on('click', function() {
    $('.signUpModalBody').waitMe();
    $('.signup_req').remove();
    $.ajax({
      method: 'POST',
      url: '<?= $this->Url->build(['controller' => 'Users', 'action' => 'signup'])?>',
      data: {
        firstname: $('[name="firstname"]').val(),
        middlename: $('[name="middlename"]').val(),
        lastname: $('[name="lastname"]').val(),
        birthdate: $('[name="birthdate"]').val(),
        gender: $('[name="gender"]').val(),
        email: $('[name="email"]').val(),
        username: $('.formInputs [name="username"]').val(),
        password: $('.formInputs [name="password"]').val(),
        conf_password: $('[name="conf_password"]').val()
      },
      headers: {
        'X-CSRF-Token': $('[name="_csrfToken"]').val()
      },
      success: function(response) {
          $('.signUpModalBody').waitMe('hide');
          response = JSON.parse(response);

          if (!response['response'] && response['error'] !== null) {
            error = response['error'];
            error_keys = Object.keys(error);

            $(error_keys).each(function() {
              field_name = this;

                if (error[field_name]['_empty'] === null || error[field_name]['_empty'] === undefined) {
                    if (error[field_name]['error'] === null || error[field_name]['error'] === undefined) {
                        if(error[field_name]['_isUnique'] === null || error[field_name]['_isUnique'] === undefined) {
                            error_message = '';
                        } else {
                            error_message = error[field_name]['_isUnique'];
                        }
                    } else {
                        error_message = error[field_name]['error'];
                    }
                } else {
                    error_message = error[field_name]['_empty'];
                }
              
              $('.formInputs [name="' + field_name + '"]').closest('div').append('<small style="color:red;" class="signup_req">' + error_message + '</small>');
            });
          } else if (!response['response'] && response['error'] !== null) {
            location.reload();
          } else {
            location.reload();
            
          }
      }
    });
  });

</script>
