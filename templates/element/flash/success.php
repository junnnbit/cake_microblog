<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>

<style>
  .alert{
    position:absolute;
    padding:0.75rem 1.25rem;
    margin-bottom:1rem;
    border:1px solid transparent;
    border-radius:0.25rem;
    z-index: 999;
    min-width: 500px;
    right: 0px;
    top: 10px;
  }
</style>
<div class="alert alert-success alert-dismissible fade show" role="alert" id="success-alert">
    <?= $message ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<script>
    $('#success-alert').fadeTo(2000, 1000).slideUp(1000, function(){
        $('#success-alert').alert('close');
    });
</script>
