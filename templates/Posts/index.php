<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<style>
    /* Pagination links */
    .pagination {
        padding-top: 5px;
    }
    .pagination a {
        color: #db0c34;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination li.active a {
        background-color: #db0c34;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}
</style>
    <aside class="left-sidebar">
        <div style="text-align:center; height: 12% !important; background-color: #ffffff;">
            <?= $this->Html->image('YNS_logo_2.jpg', ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:20%;']) ?>
        </div>
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && ($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Posts') ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-home']) . 'HOME', '/home', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && ($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Users') ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-account']) . 'PROFILE', '/profile', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
                <li class="<?= ( !empty($this->request->getAttribute('params')['action']) && ($this->request->getAttribute('params')['action'] === 'index' && $this->request->getAttribute('params')['controller'] === 'Notifications') ) ? 'active' : '' ?>">
                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-bell']) . 'NOTIFICATIONS', '/notifications', ['aria-expanded' => false, 'escape' => false]) ?>
                </li>
            </ul>
        </nav>

        <div style="text-align:center;"> 
            <button type="button" class="btn btn-lg btn-block btn-yns" data-toggle="modal" data-target="#newPostModal">New Post</button>
        </div>

        <div class="sidebar-footer" style="padding-bottom: 20px;">
            <div class="row" style="margin: 0px !important;padding: 0px !important; height: 100%;">
                <div style="padding-left: 20px; padding-top:20px;">
                    <?= $this->Html->image('profile_pics/' . h($user['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:60px; height:60px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                </div>
                <div style="padding-left: 20px; padding-top: 25px;">
                    <b><?= h($user['nickname']) === null || h($user['nickname']) === '' ? h($user['firstname']) . ' ' . h($user['lastname']) : h($user['nickname'])?></b>
                    <br/>
                    <?= '@' . h($user['username'])?>
                </div>
                <div style="padding-left: 15px; padding-top: 17px;">
                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'mdi mdi-logout', 'style' => 'font-size: 25px;']), ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]) ?>
                </div>
            </div>
            
        </div>
    </aside>
    <div class="page-wrapper" style="padding: 0px !important; height:100%; !important;">
        <div class="container-fluid" style="margin: 0px !important;padding: 0px !important; height:100%; !important;">
            <div class="row" style="margin: 0px !important;padding: 0px !important; height: 100% !important;">
                <div class="col-md-8" style="margin: 0px !important;padding: 0px !important; height: 100% !important; background-color: #ffffff0; border-right: #ebebe0 1px solid; border-left: #ebebe0 1px solid;">
                    <div style="height: 10.73% !important; padding-top: 17px; padding-left: 25px; border-bottom: #ebebe0 1px solid;">
                        <div style="height:40%">
                            <b style="font-size:20px;">Home</b>
                        </div>
                    </div>
                    <div id="posts_area" style="height: 517px;">
                        <?php $index = 0;?>
                        <?php foreach ($posts as $post) {?>
                        <?php if (h($post['posts_id']) === null) {?>
                            <div class="row" style="min-height:100px; max-height:120px; margin: 0px !important; padding-left: 25px; padding-right: 25px; padding-top:10px; padding-bottom:10px; ">
                                <div style="padding-left: 20px;">
                                    <?= $this->Html->image('profile_pics/' . h($post['user']['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                                </div>
                                <div style="padding-left: 10px; padding-top: 0px; font-size: 12px;">
                                    <b><?= h($post['user']['nickname']) === null || h($post['user']['nickname']) === '' ? h($post['user']['firstname']) . ' ' . h($post['user']['lastname']) : h($post['user']['nickname'])?></b>
                                    (<?= '@' . h($post['user']['username'])?>) · <?= date('Y', strtotime(h($post['date_created']))) === date('Y') ? date('M d', strtotime(h($post['date_created']))) : date('m/d/Y', strtotime(h($post['date_created'])))?>
                                    <br/>
                                    <?php if (count($post['images']) !== 0) {?>
                                        <small><a href="javascript:viewAttachments(<?= h($post['id'])?>, <?= $index?>)">View Attachment/s</a></small>
                                        <br/>
                                    <?php }?>
                                    <?php 
                                        $postParagraphs = explode("\n", h($post['content'])); 
                                        $maxCharPerRow = 70;
                                        $charsCounterPerRow = 0;
                                        $postString = '';
                                        
                                        for ($i = 0; $i < count($postParagraphs); $i++) {
                                            $words = explode(' ', $postParagraphs[$i]);

                                            if ($i !== 0) {
                                                $postString .= "\n";
                                            }

                                            for ($j = 0; $j < count($words); $j++) {
                                                if ($j !== 0) {
                                                    $postString .= ' ';
                                                    $charsCounterPerRow++;
                                                }

                                                $lineCount = $charsCounterPerRow + intval(strlen($words[$j]));

                                                if ($lineCount >= $maxCharPerRow) {
                                                    if (strlen($words[$j]) >= $maxCharPerRow) {
                                                        for ($k = 0; $k < strlen($words[$j]); $k++) {
                                                            if ($k % $maxCharPerRow === 0 && $k !== 0) {
                                                                $postString .= "\n";
                                                                $postString .= $words[$j][$k];
                                                                
                                                                $charsCounterPerRow = 0;
                                                            } else {
                                                                $postString .= $words[$j][$k];
                                                                
                                                                $charsCounterPerRow++;
                                                            }
                                                        }
                                                        $charsCounterPerRow = $lineCount;
                                                    } else {
                                                        $postString .= "\n";
                                                        $postString .= $words[$j];
                                                        $charsCounterPerRow = 0;
                                                    }
                                                } else {
                                                    $postString .= $words[$j];
                                                    $charsCounterPerRow = $lineCount;
                                                }
                                            }

                                            $charsCounterPerRow = 0;
                                        }

                                        echo nl2br($postString);
                                    ?>
                                </div>
                            </div>
                            <div style="border-bottom: #ebebe0 1px solid;">
                            <div class="btn-group btn-group-sm" role="group" style="width: 100%;">
                                <?php if (intval(h($post['is_liked'])) === 1) { ?>
                                    <button type="button" class="btn btn-outline-yns" onClick="unlikePost(<?= h($post['liked']['id'])?>)"><?=number_format($post['liked_count'])?> <i class="mdi mdi-heart"></i></button>
                                <?php } else {?>
                                    <button type="button" class="btn btn-outline-yns" onClick="likePost(<?= h($post['id'])?>)"><?=number_format($post['liked_count'])?> <i class="mdi mdi-heart-outline"></i></button>
                                <?php }?> 
                                <?php $commentsIdParam = base64_encode(base64_encode(base64_encode(h($post['id']))))?>
                                <?= $this->Html->link(number_format($post['comment_count']) . ' ' . $this->Html->tag('i', '', ['class' => 'mdi mdi-comment']), ['controller' => 'Comments', 'action'=>'index', $commentsIdParam], ['class' => 'btn btn-yns', 'aria-expanded' => false, 'escape' => false, 'style' => 'padding-top: 8px;']) ?>
                                <div class="btn-group show" role="group" style="width:25%;">
                                <button type="button" class="btn btn-yns" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=number_format($post['shared_count'])?> <i class="mdi mdi-share"></i></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:sharePost(<?= json_encode(h($post->id))?>, ' ', 'Repost')">Re-post Now</a>
                                    <a class="dropdown-item" href="javascript:openShareModal(<?= json_encode(h($post->id))?>, <?=$index?>)">Quote Post</a>
                                </div>
                                </div>
                                <?php if (intval(h($post['users_id'])) === intval(h($user['id']))) {?>
                                <div class="btn-group show" role="group" style="width:25%;">
                                <button type="button" class="btn btn-yns" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:openModal(<?= json_encode(h($post->id))?>, <?=$index?>)">Edit Post</a>
                                    <a class="dropdown-item" href="javascript:deletePost(<?= json_encode(h($post->id))?>)">Delete Post</a>
                                </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <?php } else {?>
                            <div class="row" style="min-height:100px; max-height:120px; margin: 0px !important; padding-left: 25px; padding-right: 25px; padding-top:10px; padding-bottom:10px; ">
                                <div style="padding-left: 20px;">
                                    <?= $this->Html->image('profile_pics/' . h($post['user']['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                                </div>
                                <div style="padding-left: 10px; padding-top: 0px; font-size: 12px;">
                                    <b><?= h($post['user']['nickname']) === null || h($post['user']['nickname']) === '' ? h($post['user']['firstname']) . ' ' . h($post['user']['lastname']) : h($post['user']['nickname'])?></b>
                                    (<?= '@' . h($post['user']['username'])?>) · <?= date('Y', strtotime(h($post['date_created']))) === date('Y') ? date('M d', strtotime(h($post['date_created']))) : date('m/d/Y', strtotime(h($post['date_created'])))?>
                                    <br/>
                                    <small><i class="mdi mdi-share"></i> Re-posted from </small> <small><a href="javascript:viewSharedPost(<?= h($post['post']['id'])?>, <?= $index?>)"><?= '@' . h($post['post']['user']['username'])?>'s post</a></small>
                                    <?php if (count($post['images']) !== 0) {?>
                                        <br/>
                                        <small><a href="javascript:viewAttachments(<?= h($post['id'])?>, <?= $index?>)">View Attachment/s</a></small>
                                    <?php }?>
                                    <div style="padding-top: 7px;"></div>
                                    <?php 
                                        $postParagraphs = explode("\n", h($post['content'])); 
                                        $maxCharPerRow = 70;
                                        $charsCounterPerRow = 0;
                                        $postString = '';
                                        
                                        for ($i = 0; $i < count($postParagraphs); $i++) {
                                            $words = explode(' ', $postParagraphs[$i]);

                                            if ($i !== 0) {
                                                $postString .= "\n";
                                            }

                                            for ($j = 0; $j < count($words); $j++) {
                                                if ($j !== 0) {
                                                    $postString .= ' ';
                                                    $charsCounterPerRow++;
                                                }

                                                $lineCount = $charsCounterPerRow + intval(strlen($words[$j]));

                                                if ($lineCount >= $maxCharPerRow) {
                                                    if (strlen($words[$j]) >= $maxCharPerRow) {
                                                        for ($k = 0; $k < strlen($words[$j]); $k++) {
                                                            if ($k % $maxCharPerRow === 0 && $k !== 0) {
                                                                $postString .= "\n";
                                                                $postString .= $words[$j][$k];
                                                                
                                                                $charsCounterPerRow = 0;
                                                            } else {
                                                                $postString .= $words[$j][$k];
                                                                
                                                                $charsCounterPerRow++;
                                                            }
                                                        }
                                                    } else {
                                                        $postString .= "\n";
                                                        $postString .= $words[$j];
                                                        $charsCounterPerRow = 0;
                                                    }
                                                } else {
                                                    $postString .= $words[$j];
                                                    $charsCounterPerRow = $lineCount;
                                                }
                                            }

                                            $charsCounterPerRow = 0;
                                        }

                                        echo nl2br($postString);
                                    ?>
                                </div>
                            </div>
                            <div style="border-bottom: #ebebe0 1px solid;">
                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example" style="width: 100%;">
                                <?php if (intval(h($post['is_liked'])) === 1) { ?>
                                    <button type="button" class="btn btn-outline-yns" onClick="unlikePost(<?= h($post['liked']['id'])?>)"><?=number_format($post['liked_count'])?> <i class="mdi mdi-heart"></i></button>
                                <?php } else {?>
                                    <button type="button" class="btn btn-outline-yns" onClick="likePost(<?= h($post['id'])?>)"><?=number_format($post['liked_count'])?> <i class="mdi mdi-heart-outline"></i></button>
                                <?php }?> 
                                <?php $commentsIdParam = base64_encode(base64_encode(base64_encode(h($post['id']))))?>
                                <?= $this->Html->link(number_format($post['comment_count']) . ' ' . $this->Html->tag('i', '', ['class' => 'mdi mdi-comment']), ['controller' => 'Comments', 'action'=>'index', $commentsIdParam], ['class' => 'btn btn-yns', 'aria-expanded' => false, 'escape' => false, 'style' => 'padding-top: 8px;']) ?>
                                <div class="btn-group show" role="group" style="width:25%;">
                                <button type="button" class="btn btn-yns" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=number_format($post['shared_count'])?> <i class="mdi mdi-share"></i></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:sharePost(<?= json_encode(h($post->posts_id))?>, ' ', 'Repost')">Re-post Now</a>
                                    <a class="dropdown-item" href="javascript:openShareModal(<?= json_encode(h($post->id))?>, <?=$index?>)">Quote Post</a>
                                </div>
                                </div>
                                <?php if (intval(h($post['users_id'])) === intval(h($user['id']))) {?>
                                <div class="btn-group show" role="group" style="width:25%;">
                                <button type="button" class="btn btn-yns" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-dots-horizontal"></i></button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="javascript:openModal(<?= json_encode(h($post->id))?>, <?=$index?>)">Edit Post</a>
                                    <a class="dropdown-item" href="javascript:deletePost(<?= json_encode(h($post->id))?>)">Delete Post</a>
                                </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php $index++;?>
                        <?php }?>
                    </div>
                    <div class="paginator">
                        <ul class="pagination"  style="padding-bottom: 0px !important; margin: 0px !important;">
                            <?= $this->Paginator->first('<< ' . __('first')) ?>
                            <?= $this->Paginator->prev('< ' . __('previous')) ?>
                            <?= $this->Paginator->numbers(array('modulus' => 4)) ?>
                            <?= $this->Paginator->next(__('next') . ' >') ?>
                            <?= $this->Paginator->last(__('last') . ' >>') ?>
                        </ul>
                        <small style="padding-left: 15px;"><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></small>
                    </div>
                </div>
                <div class="col-md-4" style="margin: 0px !important;padding: 0px !important;height: 100% !important;">
                    <div style="text-align:center; height: 9.4% !important; padding-top: 14px; position: relative">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'search'], 'type' => 'get'])?>
                        <?= $this->Form->control('key', ['label' => false, 'class' => 'form-control', 'placeholder' => 'search...', 'style' => 'width: 80%; display: inline-block; padding-top: 3px;', 'value' => $this->request->getQuery('key')])?>
                        <?= $this->Form->end()?>
                    </div>
                    <div style="padding-top:7px;border-bottom: #ebebe0 1px solid;"></div>
                    <!-- <hr> -->
                    <div style="height: 7% !important; padding-top: 15px; padding-left: 25px;">
                        <h5>WHO TO FOLLOW</h5>
                    </div>
                    <div style="padding-top: 7px; border-bottom: #ebebe0 1px solid"></div>
                    <?php foreach ($users as $toFollow) {?>
                        <div class="row" style="height:80px; margin: 0px !important; padding-left: 25px; padding-right: 25px; padding-top:20px; padding-bottom:10px; border-bottom: #ebebe0 1px solid;">
                            <div style="padding-left: 5px;">
                                <?= $this->Html->image('profile_pics/' . h($toFollow['profile_pic']), ['alt' => 'Image', 'class' => 'img-fluid', 'style' => 'width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;']) ?>
                            </div>
                            <div style="padding-left: 10px; padding-top: 0px; font-size: 12px;">
                                <?php $idParam = base64_encode(base64_encode(base64_encode(h($toFollow['id']))))?>
                                <b><?= $this->Html->link( h($toFollow['nickname']) === null || h($toFollow['nickname']) === '' ? h($toFollow['firstname']) . ' ' . h($toFollow['lastname']) : h($toFollow['nickname']), ['controller' => 'Users', 'action' => 'user', $idParam]) ?></b>
                                <br/>(<?= '@' . h($toFollow['username'])?>)
                            </div>
                            <div style="padding-left: 50px; padding-top: 3px;">
                                <button class="btn btn-sm btn-yns" onClick="follow(<?= h($toFollow['id'])?>)">Follow</button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!---content---->

<!-- Modal -->
<div class="modal fade" id="newPostModal" tabindex="-1" role="dialog" aria-labelledby="newPostModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newPostModalLabel">What's happening?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, ['type' => 'file', 'class' => 'newPostForm'])?>
      <div class="modal-body newPostModalBody">
        <div class="row formPostInputs">
          <!-- Post -->
          <div class="col-md-12">
            <textarea class="form-control required" rows = "5" cols = "50" name = "content"></textarea>
            <label id="content_count" style="padding-top:5px;">140</label>
          </div>
          <!-- Post -->
          <div class="col-md-12">
            <label class="active">Photos</label>
            <div class="input-images" style="padding-top: .5rem;"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?= $this->Form->button(__('POST!'), ['type' => 'submit', 'class' => 'btn btn-yns submit_post'])?>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="editPostModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editPostModalLabel">Edit Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, ['type' => 'file', 'class' => 'editPostForm'])?>
      <div class="modal-body editPostModalBody">
        <div class="row formePostInputs">
          <input type="hidden" name="posts_id">
          <!-- Post -->
          <div class="col-md-12">
            <textarea class="form-control required" rows = "5" cols = "50" name = "content"></textarea>
            <label id="econtent_count" style="padding-top:5px;">140</label>
          </div>
          <div class="col-md-12">
            <label class="active">Photos</label>
            <div class="input-images-edit" style="padding-top: .5rem;"></div>
          </div>
          <!-- Post -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?= $this->Form->button(__('POST!'), ['type' => 'submit', 'class' => 'btn btn-yns submit_epost'])?>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="sharePostModal" tabindex="-1" role="dialog" aria-labelledby="sharePostModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sharePostModalLabel">What's happening?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?= $this->Form->create(null, ['type' => 'file', 'class' => 'sharePostForm'])?>
      <div class="modal-body sharePostModalBody">
        <div class="row formsPostInputs">
        <input type="hidden" name="posts_id">
          <!-- Post -->
          <div class="col-md-12">
            <textarea class="form-control required" rows = "5" cols = "50" name = "content"></textarea>
            <label id="scontent_count" style="padding-top:5px;">140</label>
          </div>
          <div class="col-md-12">
            <label class="active">Photos</label>
            <div class="input-images-share" style="padding-top: .5rem;"></div>
          </div>
          <!-- Post -->
        </div>
        <div style="padding-top:20px;"></div>
        <div id="orginal_post_content" style="border-radius: 0.25rem; border: 1px solid #ced4da; padding: 10px 20px 10px;">
            <small><i class="mdi mdi-share"></i> Re-post from</small>
            <br/>
            <div style="display: inline-block;">
                <div style="display: inline-block;">
                    <img src="" alt="Image" class="img-fluid" style="width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;" id="share_post_pic">
                </div>
                <div style="padding-left: 10px; font-size: 12px; display: inline-block;">
                    <b id="share_post_name" style="display: inline-block"></b>
                    <div id="share_post_user_date" style="display: inline-block"></div>
                </div>
                <br/>
            </div>
            <div style="padding-top:10px;"></div>
            <div id="share_post_content" style="font-size:13px;"></div>
            <div style="padding-top:10px;"></div>
            <div id="share_post_images" style="text-align: center;"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <?= $this->Form->button(__('POST!'), ['type' => 'button', 'class' => 'btn btn-yns submit_share_post'])?>
      </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="vsharePostModal" tabindex="-1" role="dialog" aria-labelledby="vsharePostModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="vsharePostModalLabel">View Full Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body vsharePostModalBody">
        <div id="vshared_posted" style="">
            <div style="display: inline-block;">
                <div style="display: inline-block; padding-left:12px;">
                    <img src="" alt="Image" class="img-fluid" style="width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;" id="vpic">
                </div>
                <div style="padding-left: 10px; font-size: 12px; display: inline-block;">
                    <b id="vname" style="display: inline-block"></b>
                    <div id="vdate" style="display: inline-block"></div>
                </div>
                <br/>
            </div>
            <div style="padding-top:10px;"></div>
            <div id="vcontent" style="font-size:13px; padding-left:15px;"></div>
            <div style="padding-top:10px;"></div>
            <div id="vimages" style="text-align: center;"></div>
        </div>
        <div style="padding-top:20px;"></div>
        <div id="vorginal_post_content" style="border-radius: 0.25rem; border: 1px solid #ced4da; padding: 10px 20px 10px;">
            <small><i class="mdi mdi-share"></i> Re-posted from</small>
            <br/>
            <div style="display: inline-block;">
                <div style="display: inline-block;">
                    <img src="" alt="Image" class="img-fluid" style="width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;" id="vshare_post_pic">
                </div>
                <div style="padding-left: 10px; font-size: 12px; display: inline-block;">
                    <b id="vshare_post_name" style="display: inline-block"></b>
                    <div id="vshare_post_user_date" style="display: inline-block"></div>
                </div>
                <br/>
            </div>
            <div style="padding-top:10px;"></div>
            <div id="vshare_post_content" style="font-size:13px;"></div>
            <div style="padding-top:10px;"></div>
            <div id="vshare_post_images" style="text-align: center;"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="vAttachmentsModal" tabindex="-1" role="dialog" aria-labelledby="vAttachmentsModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="vAttachmentsModalLabel">View Attachments</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body vAttachmentsModalBody">
        <div id="vAttachmentContent" style="">
            <div style="display: inline-block;">
                <div style="display: inline-block; padding-left:12px;">
                    <img src="" alt="Image" class="img-fluid" style="width:40px; height:40px; border-radius: 50px; border: #ebebe0 1px solid;" id="vattachment_pic">
                </div>
                <div style="padding-left: 10px; font-size: 12px; display: inline-block;">
                    <b id="vattachment_name" style="display: inline-block"></b>
                    <div id="vattachment_date" style="display: inline-block"></div>
                </div>
                <br/>
            </div>
            <div style="padding-top:10px;"></div>
            <div id="vattachment_content" style="font-size:13px; padding-left:15px;"></div>
            <div style="padding-top:10px;"></div>
            <div id="vattachment_images" style="text-align: center;"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
    var images = $('.input-images').imageUploader({
        label: 'Drag & Drop files here or click to browse',
        maxFiles: 4,
        extensions: ['.jpg', '.jpeg', '.png', '.gif'],
    });

    var images_share = $('.input-images-share').imageUploader({
        label: 'Drag & Drop files here or click to browse',
        maxFiles: 4,
        extensions: ['.jpg', '.jpeg', '.png', '.gif'],
    });

    var posts = <?php echo json_encode($posts)?>;
    var months = [
                'Jan.', 
                'Feb.',
                'Mar.',
                'Apr.',
                'May',
                'Jun.',
                'Jul.',
                'Aug.',
                'Sept.',
                'Oct.',
                'Nov.',
                'Dec.'
             ];

    $(".newPostForm textarea[name='content']").on("keyup change paste", function(event){
        var count = countChar(this,140);
        $("#content_count").html(count);
    });

    $(".editPostForm textarea[name='content']").on("keyup change paste", function(event){
        var count = countChar(this,140);
        $("#econtent_count").html(count);
    });

    $(".sharePostForm textarea[name='content']").on("keyup change paste", function(event){
        var count = countChar(this,140);
        $("#scontent_count").html(count);
    });

    $('#newPostModal').on('hidden.bs.modal', function() {
        $('.newPostForm textarea[name="content"]').val('');
        $("#content_count").html(140);
        $('.input-images').html('');
        images_share = $('.input-images').imageUploader({
            label: 'Drag & Drop files here or click to browse',
            maxFiles: 4,
            extensions: ['.jpg', '.jpeg', '.png', '.gif'],
        });
        $(".post_req").remove();
    });
    $('#editPostModal').on('hidden.bs.modal', function() {
        $('.editPostForm textarea[name="content"]').val('');
        $("#econtent_count").html(140);
        $('.input-images-edit').html('');
        $(".epost_req").remove();
    });
    $('#sharePostModal').on('hidden.bs.modal', function() {
        $('.sharePostForm textarea[name="content"]').val('');
        $("#scontent_count").html(140);
        $('.input-images-share').html('');
        images_share = $('.input-images-share').imageUploader({
            label: 'Drag & Drop files here or click to browse',
            maxFiles: 4,
            extensions: ['.jpg', '.jpeg', '.png', '.gif'],
        });
        $(".spost_req").remove();
    });

    $('.newPostForm').on('submit', function(e) {
        e.preventDefault();
        $('.newPostModalBody').waitMe();
        $(".post_req").remove();
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: new FormData(this),
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        processData: false,
        contentType: false,
        success: function(response) {
            $('.newPostModalBody').waitMe('hide');
            
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                if (error['content']) {
                    if (error['content']['_empty']) {
                        error_message = error['content']['_empty'];
                    } else {
                        error_message = error['content']['maxLength'];
                    }
                    $('.newPostForm [name="content"]').closest('div').append('<small style="color:red;" class="post_req">' + error_message + '</small>');
                }
                if (error['images']) {
                    $('div.input-images').closest('div').append('<small style="color:red;" class="post_req">Please double check attached file/s. Only image files of type jpeg/jpg/png will only be accepted. The image file size must be less than or equal to 5MB.</small>');
                }
            } else {
                location.reload();
            }
        }
        });
    });

    $('.editPostForm').on('submit', function(e) {
        e.preventDefault();
        $('.editPostModalBody').waitMe();
        $(".epost_req").remove();
        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'edit'])?>',
        data: new FormData(this),
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        processData: false,
        contentType: false,
        success: function(response) {
            $('.editPostModalBody').waitMe('hide');
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                if (error['content']) {
                    error_message = error['content']['_empty'];
                    $('.editPostForm [name="content"]').closest('div').append('<small style="color:red;" class="epost_req">' + error_message + '</small>');
                }
                if (error['images']) {
                    $('div.input-images-edit').closest('div').append('<small style="color:red;" class="epost_req">Please double check attached file/s. Only image files of type jpeg/jpg/png will only be accepted. The image file size must be less than or equal to 5MB.</small>');
                }
            } else {
                location.reload();
            }
        }
        });
    });

    $('.submit_share_post').on('click', function() {
        $('.sharePostModalBody').waitMe();
        $(".spost_req").remove();
        content = $('.sharePostForm [name="content"]').val();
        id =  $('.sharePostForm input[name="posts_id"]').val();
        sharePost(id, content, '');
        $('.sharePostModalBody').waitMe('hide');
    });

    function sharePost(id, content, action) {
        if (action === 'Repost') {
            data = {
                content: content,
                posts_id: id,
                action: action
            };
            processData = true;
            contentType = 'application/x-www-form-urlencoded';
        } else {
            data = new FormData($('.sharePostForm')[0]);
            processData = false;
            contentType = false;
        }

        $.ajax({
        method: 'POST',
        url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'add'])?>',
        data: data,
        headers: {
            'X-CSRF-Token': $('[name="_csrfToken"]').val()
        },
        processData: processData,
        contentType: contentType,
        success: function(response) {
            response = JSON.parse(response);

            if (!response['response'] && response['error'] !== null) {
                error = response['error'];
                if (error['content']) {
                    error_message = error['content']['_empty'];
                    $('.sharePostForm [name="content"]').closest('div').append('<small style="color:red;" class="spost_req">' + error_message + '</small>');
                }
                if (error['images']) {
                    $('div.input-images-share').closest('div').append('<small style="color:red;" class="spost_req">Please double check attached file/s. Only image files of type jpeg/jpg/png will only be accepted. The image file size must be less than or equal to 5MB.</small>');
                } 
            } else {
                location.reload();
            }
        }
        });
    }

    function openModal(id, index) {
        if ($('#editPostModal').modal('show')) {
            $('.editPostForm input[name="posts_id"]').val(escapeHtml(id));
            $('.editPostForm [name="content"]').val(escapeHtml(posts[index]['content']));
            $("#econtent_count").html(140 - $('.editPostForm [name="content"]').val().length);
            var editUploadedImages = posts[index]['images'].map(
                function(index) {
                    rootLink = '/img/posts_images/';
                    return {id: escapeHtml(index.post_id) + '-' + escapeHtml(index.id), src: rootLink + escapeHtml(index.image)};
                }
            );
            
            var images_edit = $('.input-images-edit').imageUploader({
                label: 'Drag & Drop files here or click to browse',
                maxFiles: 4,
                extensions: ['.jpg', '.jpeg', '.png', '.gif'],
                preloaded: editUploadedImages
            });
        }
    }

    function viewAttachments(id, index) {
        if ($('#vAttachmentsModal').modal('show')) {
            nickname = posts[index]['user']['nickname'] === null || posts[index]['user']['nickname'] === '' ? escapeHtml(posts[index]['user']['firstname']) + ' ' + escapeHtml(posts[index]['user']['lastname']) : escapeHtml(posts[index]['user']['nickname']);
            post_date = new Date(escapeHtml(posts[index]['date_created'])).getFullYear() === new Date().getFullYear() ? (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate()) : (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate() + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getFullYear()) ;
            rootLink = '/img/';
            $('#vattachment_pic').attr('src', rootLink + 'profile_pics/' + escapeHtml(posts[index]['user']['profile_pic']));
            $('#vattachment_name').html(nickname);
            $('#vattachment_date').html('(@' + escapeHtml(posts[index]['user']['username']) + ')' + ' · ' + post_date);
            vattachment_content = (escapeHtml(posts[index]['content'])).split(/[\r\n]+/);
            maxCharPerRow = 58;
            charsRowCounter = 0;
            vcontentStr = '';

            for (i = 0; i < vattachment_content.length; i++) {
                words = vattachment_content[i].split(' ');
                if (i !== 0) {
                    vcontentStr += "<br/>";
                }

                for (j = 0; j < words.length; j++) {
                    if (j !== 0) {
                        vcontentStr += ' ';
                        charsRowCounter++;
                    }

                    lineCount = charsRowCounter + parseInt(words[j].length);

                    if (lineCount >= maxCharPerRow) {
                        if (words[j].length >= maxCharPerRow) {
                            for (k = 0; k < words[j].length; k++) {
                                if (k % maxCharPerRow === 0 && k !== 0) {
                                    vcontentStr += "<br/>";
                                    vcontentStr += words[j][k];
                                    charsRowCounter = 0;
                                } else {
                                    vcontentStr += words[j][k];
                                    charsRowCounter++;
                                }
                            }
                        } else {
                            vcontentStr += "<br/>";
                            vcontentStr += words[j];
                            charsRowCounter = 0;
                        }
                    } else {
                        vcontentStr += words[j];
                        charsRowCounter = lineCount;
                    }
                }
                charsRowCounter = 0;
            }
            $('#vattachment_content').html(vcontentStr);

            vattachment_images = '';

            for (let imgIndex = 0; imgIndex < posts[index]['images'].length; imgIndex++) {
                vattachment_images += '<img src="' + rootLink + 'posts_images/' + escapeHtml(posts[index]['images'][imgIndex]['image']) + '" style="width:225px;height:225px;">';
            }

            $('#vattachment_images').html(vattachment_images);

        }
    }

    function openShareModal(id, index) {
        if ($('#sharePostModal').modal('show')) {
            if (posts[index]['posts_id'] === null) {
                $('input[name="posts_id"]').val(escapeHtml(posts[index]['id']));
                nickname = posts[index]['user']['nickname'] === null || posts[index]['user']['nickname'] === '' ? escapeHtml(posts[index]['user']['firstname']) + ' ' + escapeHtml(posts[index]['user']['lastname']) : escapeHtml(posts[index]['user']['nickname']);
                post_date = new Date(escapeHtml(posts[index]['date_created'])).getFullYear() === new Date().getFullYear() ? (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate()) : (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate() + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getFullYear()) ;
                rootLink = '/img/';
                $('#share_post_pic').attr('src', rootLink + 'profile_pics/' + escapeHtml(posts[index]['user']['profile_pic']));
                $('#share_post_name').html(nickname);
                $('#share_post_user_date').html('(@' + escapeHtml(posts[index]['user']['username']) + ')' + ' · ' + post_date);
                share_post_content = (escapeHtml(posts[index]['content'])).split(/[\r\n]+/);
                maxCharPerRow = 58;
                charsRowCounter = 0;
                vcontentStr = '';

                for (i = 0; i < share_post_content.length; i++) {
                    words = share_post_content[i].split(' ');
                    if (i !== 0) {
                        vcontentStr += "<br/>";
                    }

                    for (j = 0; j < words.length; j++) {
                        if (j !== 0) {
                            vcontentStr += ' ';
                            charsRowCounter++;
                        }

                        lineCount = charsRowCounter + parseInt(words[j].length);

                        if (lineCount >= maxCharPerRow) {
                            if (words[j].length >= maxCharPerRow) {
                                for (k = 0; k < words[j].length; k++) {
                                    if (k % maxCharPerRow === 0 && k !== 0) {
                                        vcontentStr += "<br/>";
                                        vcontentStr += words[j][k];
                                        charsRowCounter = 0;
                                    } else {
                                        vcontentStr += words[j][k];
                                        charsRowCounter++;
                                    }
                                }
                            } else {
                                vcontentStr += "<br/>";
                                vcontentStr += words[j];
                                charsRowCounter = 0;
                            }
                        } else {
                            vcontentStr += words[j];
                            charsRowCounter = lineCount;
                        }
                    }
                    charsRowCounter = 0;
                }
                $('#share_post_content').html(vcontentStr);
                share_post_images = '';
                if (posts[index]['images'].length !== 0) {
                    for (let shareImagesIndex = 0; shareImagesIndex < posts[index]['images'].length; shareImagesIndex++) {
                        share_post_images += '<img src="' + rootLink + 'posts_images/' + escapeHtml(posts[index]['images'][shareImagesIndex]['image']) + '" style="width:210px;height:210px;">';
                    }
                }
                $('#share_post_images').html(share_post_images);
            } else {
                $('input[name="posts_id"]').val(escapeHtml(posts[index]['posts_id']));
                nickname = posts[index]['post']['user']['nickname'] === null || posts[index]['post']['user']['nickname'] === '' ? escapeHtml(posts[index]['post']['user']['firstname']) + ' ' + escapeHtml(posts[index]['post']['user']['lastname']) : escapeHtml(posts[index]['post']['user']['nickname']);
                post_date = new Date(escapeHtml(posts[index]['post']['date_created'])).getFullYear() === new Date().getFullYear() ? (months[new Date(escapeHtml(posts[index]['post']['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getDate()) : (months[new Date(escapeHtml(posts[index]['post']['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getDate() + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getFullYear()) ;
                rootLink = '/img/';
                $('#share_post_pic').attr('src', rootLink + 'profile_pics/' + escapeHtml(posts[index]['post']['user']['profile_pic']));
                $('#share_post_name').html(nickname);
                $('#share_post_user_date').html('(@' + escapeHtml(posts[index]['post']['user']['username']) + ')' + ' · ' + post_date);
                if (escapeHtml(posts[index]['post']['is_deleted']) === '1') {
                    $('#share_post_content').html('[ This content is not available ]');
                } else {
                    share_post_content = (escapeHtml(posts[index]['post']['content'])).split(/[\r\n]+/);
                    maxCharPerRow = 58;
                    charsRowCounter = 0;
                    vcontentStr = '';

                    for (i = 0; i < share_post_content.length; i++) {
                        words = share_post_content[i].split(' ');
                        if (i !== 0) {
                            vcontentStr += "<br/>";
                        }

                        for (j = 0; j < words.length; j++) {
                            if (j !== 0) {
                                vcontentStr += ' ';
                                charsRowCounter++;
                            }

                            lineCount = charsRowCounter + parseInt(words[j].length);

                            if (lineCount >= maxCharPerRow) {
                                if (words[j].length >= maxCharPerRow) {
                                    for (k = 0; k < words[j].length; k++) {
                                        if (k % maxCharPerRow === 0 && k !== 0) {
                                            vcontentStr += "<br/>";
                                            vcontentStr += words[j][k];
                                            charsRowCounter = 0;
                                        } else {
                                            vcontentStr += words[j][k];
                                            charsRowCounter++;
                                        }
                                    }
                                } else {
                                    vcontentStr += "<br/>";
                                    vcontentStr += words[j];
                                    charsRowCounter = 0;
                                }
                            } else {
                                vcontentStr += words[j];
                                charsRowCounter = lineCount;
                            }
                        }
                        charsRowCounter = 0;
                    }
                    $('#share_post_content').html(vcontentStr);
                }
                share_post_images = '';
                if (posts[index]['post']['images'].length !== 0) {
                    for (let shareImagesIndex = 0; shareImagesIndex < posts[index]['post']['images'].length; shareImagesIndex++) {
                        share_post_images += '<img src="' + rootLink + 'posts_images/' + escapeHtml(posts[index]['post']['images'][shareImagesIndex]['image']) + '" style="width:210px;height:210px;">';
                    }
                }
                $('#share_post_images').html(share_post_images);
            }
        }
    }

    function viewSharedPost(id, index) {
        if ($('#vsharePostModal').modal('show')) {
            $('input[name="vpost_id"]').val(escapeHtml(posts[index]['posts_id']));
            rootLink = '/img/';

            vnickname = posts[index]['user']['nickname'] === null || posts[index]['user']['nickname'] === '' ? escapeHtml(posts[index]['user']['firstname']) + ' ' + escapeHtml(posts[index]['user']['lastname']) : escapeHtml(posts[index]['user']['nickname']);
            vpost_date = new Date(escapeHtml(posts[index]['date_created'])).getFullYear() === new Date().getFullYear() ? (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate()) : (months[new Date(escapeHtml(posts[index]['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getDate() + ' ' + new Date(escapeHtml(posts[index]['date_created'])).getFullYear()) ;
            $('#vpic').attr('src', rootLink + 'profile_pics/' + escapeHtml(posts[index]['user']['profile_pic']));
            $('#vname').html(vnickname);
            $('#vdate').html('(@' + escapeHtml(posts[index]['user']['username']) + ')' + ' · ' + vpost_date);
            vcontentParagraphs = (escapeHtml(posts[index]['content'])).split(/[\r\n]+/);
            maxCharPerRow = 58;
            charsRowCounter = 0;
            vcontentStr = '';

            for (i = 0; i < vcontentParagraphs.length; i++) {
                words = vcontentParagraphs[i].split(' ');
                if (i !== 0) {
                    vcontentStr += "<br/>";
                }

                for (j = 0; j < words.length; j++) {
                    if (j !== 0) {
                        vcontentStr += ' ';
                        charsRowCounter++;
                    }

                    lineCount = charsRowCounter + parseInt(words[j].length);

                    if (lineCount >= maxCharPerRow) {
                        if (words[j].length >= maxCharPerRow) {
                            for (k = 0; k < words[j].length; k++) {
                                if (k % maxCharPerRow === 0 && k !== 0) {
                                    vcontentStr += "<br/>";
                                    vcontentStr += words[j][k];
                                    charsRowCounter = 0;
                                } else {
                                    vcontentStr += words[j][k];
                                    charsRowCounter++;
                                }
                            }
                        } else {
                            vcontentStr += "<br/>";
                            vcontentStr += words[j];
                            charsRowCounter = 0;
                        }
                    } else {
                        vcontentStr += words[j];
                        charsRowCounter = lineCount;
                    }
                }
                charsRowCounter = 0;
            }
            $('#vcontent').html(vcontentStr);
            vimages = '';
            if (posts[index]['images'].length !== 0) {
                for (let imagesIndex = 0; imagesIndex < posts[index]['images'].length; imagesIndex++) {
                    vimages += '<img src="' + rootLink + 'posts_images/' + escapeHtml(posts[index]['images'][imagesIndex]['image']) + '" style="width:225px;height:225;">';
                }
            }
            $('#vimages').html(vimages);

            nickname = posts[index]['post']['user']['nickname'] === null || posts[index]['post']['user']['nickname'] === '' ? escapeHtml(posts[index]['post']['user']['firstname']) + ' ' + escapeHtml(posts[index]['post']['user']['lastname']) : escapeHtml(posts[index]['post']['user']['nickname']);
            post_date = new Date(escapeHtml(posts[index]['post']['date_created'])).getFullYear() === new Date().getFullYear() ? (months[new Date(escapeHtml(posts[index]['post']['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getDate()) : (months[new Date(escapeHtml(posts[index]['post']['date_created'])).getMonth()] + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getDate() + ' ' + new Date(escapeHtml(posts[index]['post']['date_created'])).getFullYear()) ;
            $('#vshare_post_pic').attr('src', rootLink + 'profile_pics/' + escapeHtml(posts[index]['post']['user']['profile_pic']));
            $('#vshare_post_name').html(nickname);
            $('#vshare_post_user_date').html('(@' + escapeHtml(posts[index]['post']['user']['username']) + ')' + ' · ' + post_date);
            if (escapeHtml(posts[index]['post']['is_deleted']) === '1') {
                $('#vshare_post_content').html('[ This content is not available ]');
            } else {
                vshare_post_content = (escapeHtml(posts[index]['post']['content'])).split(/[\r\n]+/);
                maxCharPerRow = 58;
                charsRowCounter = 0;
                vcontentStr = '';

                for (i = 0; i < vshare_post_content.length; i++) {
                    words = vshare_post_content[i].split(' ');
                    if (i !== 0) {
                        vcontentStr += "<br/>";
                    }

                    for (j = 0; j < words.length; j++) {
                        if (j !== 0) {
                            vcontentStr += ' ';
                            charsRowCounter++;
                        }

                        lineCount = charsRowCounter + parseInt(words[j].length);

                        if (lineCount >= maxCharPerRow) {
                            if (words[j].length >= maxCharPerRow) {
                                for (k = 0; k < words[j].length; k++) {
                                    if (k % maxCharPerRow === 0 && k !== 0) {
                                        vcontentStr += "<br/>";
                                        vcontentStr += words[j][k];
                                        charsRowCounter = 0;
                                    } else {
                                        vcontentStr += words[j][k];
                                        charsRowCounter++;
                                    }
                                }
                            } else {
                                vcontentStr += "<br/>";
                                vcontentStr += words[j];
                                charsRowCounter = 0;
                            }
                        } else {
                            vcontentStr += words[j];
                            charsRowCounter = lineCount;
                        }
                    }
                    charsRowCounter = 0;
                }
                $('#vshare_post_content').html(vcontentStr);
            }
            vshare_post_images = '';
            if (posts[index]['post']['images'].length !== 0) {
                for (let sharedImagesIndex = 0; sharedImagesIndex < posts[index]['post']['images'].length; sharedImagesIndex++) {
                    vshare_post_images += '<img src="' + rootLink + 'posts_images/' + escapeHtml(posts[index]['post']['images'][sharedImagesIndex]['image']) + '" style="width:210px;height:210px;">';
                }
            }
            $('#vshare_post_images').html(vshare_post_images);
        }
    }

    function deletePost(id) {
        Swal.fire({
            title: "Are you sure you want to delete this post?",
            text: "",
            icon: "warning",
            showCancelButton: true
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
            method: 'POST',
            url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'delete'])?>',
            data: {
                posts_id: id
            },
            headers: {
                'X-CSRF-Token': $('[name="_csrfToken"]').val()
            },
            success: function(response) {
                location.reload();
            }
            });
        }
        });
    }

    function follow(id) {
        $.ajax({
            method: 'POST',
            url: '<?= $this->Url->build(['controller' => 'FollowersFollowing', 'action' => 'add'])?>',
            data: {
                following_id: id
            },
            headers: {
                'X-CSRF-Token': $('[name="_csrfToken"]').val()
            },
            success: function(response) {
                location.reload();
            }
        });
    }

    function likePost(id) {
        $.ajax({
            method: 'POST',
            url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'likePost'])?>',
            data: {
                posts_id: id
            },
            headers: {
                'X-CSRF-Token': $('[name="_csrfToken"]').val()
            },
            success: function(response) {
                location.reload();
            }
        });
    }

    function unlikePost(id) {
        $.ajax({
            method: 'POST',
            url: '<?= $this->Url->build(['controller' => 'Posts', 'action' => 'unlikePost'])?>',
            data: {
                id: id
            },
            headers: {
                'X-CSRF-Token': $('[name="_csrfToken"]').val()
            },
            success: function(response) {
                location.reload();
            }
        });
    }
 
    //COUNT TEXT CHARACTER
    function countChar(txtId,maxLength){
        var cs = $(txtId).val().length;
        
        if(parseInt(cs) > parseInt(maxLength)){
            var content = $(txtId).val();
            var c = content.substr(0, maxLength);
            $(txtId).val(c);
            cs = $(txtId).val().length;
        }
        
        const count = (str) => {
            const re = /\n/g
            return ((str || '').match(re) || []).length
        }

        newLineCount = count($(txtId).val());

        var cc = parseInt(maxLength) - (parseInt(cs) + parseInt(newLineCount));

        return cc;
    }

    function escapeHtml(text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        
        return (text.toString()).replace(/[&<>]/g, function(m) { return map[m]; });
    }
</script>
