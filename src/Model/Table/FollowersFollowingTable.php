<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FollowersFollowing Model
 *
 * @property \App\Model\Table\FollowersTable&\Cake\ORM\Association\BelongsTo $Followers
 * @property \App\Model\Table\FollowingsTable&\Cake\ORM\Association\BelongsTo $Followings
 *
 * @method \App\Model\Entity\FollowersFollowing newEmptyEntity()
 * @method \App\Model\Entity\FollowersFollowing newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\FollowersFollowing[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FollowersFollowing get($primaryKey, $options = [])
 * @method \App\Model\Entity\FollowersFollowing findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\FollowersFollowing patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FollowersFollowing[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\FollowersFollowing|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FollowersFollowing saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class FollowersFollowingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('followers_following');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', ['className' => 'Users',
            'foreignKey' => 'follower_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users1', [ 'className' => 'Users',
            'foreignKey' => 'following_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_deleted')
            ->allowEmptyString('is_deleted');

        $validator
            ->dateTime('date_deleted')
            ->allowEmptyDateTime('date_deleted');

        $validator
            ->dateTime('date_created')
            ->allowEmptyDateTime('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        return $rules;
    }
}
