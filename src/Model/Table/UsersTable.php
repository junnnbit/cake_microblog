<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 255)
            ->notEmptyString('firstname', 'Please fill this field.')
            ->add('firstname', 'error', 
            [
                'rule' => function($value, $context){
                    if (preg_match('/^[a-zA-Z._ \-]+[a-zA-Z0-9._ \-]+$/', $value)) {
                        return true;
                    } else {
                        return 'Invalid Input. Only accepts alpha-numeric characters.';
                    }
                }
            ]
        );

        $validator
            ->scalar('middlename')
            ->maxLength('middlename', 255)
            ->allowEmptyString('middlename')
            ->add('middlename', 'error', 
            [
                'rule' => function($value, $context){
                    if (preg_match('/^[a-zA-Z._ \-]+[a-zA-Z0-9._ \-]+$/', $value)) {
                        return true;
                    } else {
                        return 'Invalid Input. Only accepts alpha-numeric characters.';
                    }
                }
            ]);

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255)
            ->notEmptyString('lastname', 'Please fill this field.')
            ->add('lastname', 'error', 
            [
                'rule' => function($value, $context){
                    if (preg_match('/^[a-zA-Z._ \-]+[a-zA-Z0-9._ \-]+$/', $value)) {
                        return true;
                    } else {
                        return 'Invalid Input. Only accepts alpha-numeric characters.';
                    }
                }
            ]);

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 255)
            ->allowEmptyString('nickname')
            ->add('nickname', 'error', 
            [
                'rule' => function($value, $context){
                    if (preg_match('/^[a-zA-Z._ \-]+[a-zA-Z0-9._ \-]+$/', $value)) {
                        return true;
                    } else {
                        return 'Invalid Input. Only accepts alpha-numeric characters.';
                    }
                }
            ]);

        $validator
            ->scalar('gender')
            ->maxLength('gender', 255)
            ->notEmptyString('gender', 'Please fill this field.');

        $validator
            ->date('birthdate')
            ->notEmptyString('birthdate', 'Please fill this field.')
            ->add('birthdate', 'error', 
            [
                'rule' => function($value, $context){
                    $birthDate = date('Y-m-d', strtotime($value));
                    $todayDate = date('Y-m-d');

                    $diff = date_diff(date_create($birthDate), date_create($todayDate));
                    $age = $diff->format('%y');

                    if ($age < 18) {
                        return 'You should be 18+ years of age to create an account.';
                    } else {
                        return true;
                    }
                }
            ]);

        $validator
            ->scalar('location')
            ->maxLength('location', 255)
            ->allowEmptyString('location');

        $validator
            ->scalar('contact_num')
            ->maxLength('contact_num', 50)
            ->allowEmptyString('contact_num');

        $validator
            ->email('email')
            ->notEmptyString('email', 'Please fill this field.')
            ->add('email', 'error', ['rule' => 'email', 'message' => 'Provided email address is invalid.']);

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->notEmptyString('username', 'Please fill this field.')
            ->add('username', 'error', 
            [
                'rule' => function($value, $context){
                    if (preg_match('/^[a-zA-Z._ \-]+[a-zA-Z0-9._ \-]+$/', $value)) {
                        return true;
                    } else {
                        return 'Invalid Input. Only accepts alpha-numeric characters.';
                    }
                }
            ]);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->notEmptyString('password', 'Please fill this field.')
            ->add('password', 'error', 
                [
                    'rule' => function($value, $context){
                        $password = $value;

                        //password validation
                        $upperCaseRegEx = '@[A-Z]@';
                        $lowerCaseRegEx = '@[a-z]@';
                        $numberRegEx = '@[0-9]@';
                        $minCount = 8;

                        $upperCase = preg_match($upperCaseRegEx, $password);
                        $lowerCase = preg_match($lowerCaseRegEx, $password);
                        $number    = preg_match($numberRegEx, $password);

                        if (!$upperCase || !$lowerCase || !$number || strlen($password) < $minCount) {
                            return 'Password should be at least 8 characters in length and should include at least one upper case letter and one number.';
                        } else {
                            return true;
                        }
                    }
                ]
            );

        $validator
            ->scalar('bio')
            ->maxLength('bio', 255)
            ->allowEmptyString('bio');

        $validator
            ->scalar('profile_pic')
            ->maxLength('profile_pic', 255)
            ->allowEmptyFile('profile_pic');

        $validator
            ->allowEmptyFile('profile_pic_raw')
            ->add('profile_pic_raw',  [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpg', 'image/jpeg', 'image/png', 'image/svg+xml']],
                    'message' => 'File type cannot be uploaded. Only accepts image files with extensions of .jpeg, .jpg and .png'
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '5MB'],
                    'message' => 'Image file size must be less than or equal to 5MB.'
                ]
            ]);

        $validator
            ->scalar('header_pic')
            ->maxLength('header_pic', 255)
            ->allowEmptyString('header_pic');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->allowEmptyString('status');

        $validator
            ->boolean('is_private')
            ->allowEmptyString('is_private');

        $validator
            ->scalar('hash_code')
            ->maxLength('hash_code', 255)
            ->allowEmptyString('hash_code');

        $validator
            ->boolean('is_verified')
            ->allowEmptyString('is_verified');

        $validator
            ->dateTime('date_verified')
            ->allowEmptyDateTime('date_verified');

        $validator
            ->dateTime('date_modified')
            ->allowEmptyDateTime('date_modified');

        $validator
            ->boolean('is_deleted')
            ->allowEmptyString('is_deleted');

        $validator
            ->dateTime('date_deleted')
            ->allowEmptyDateTime('date_deleted');

        $validator
            ->dateTime('date_created')
            ->allowEmptyDateTime('date_created');

        $validator
            ->notEmptyString('conf_password', 'Please fill this field.')
            ->add('conf_password', 'error', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Passwords are not equal',
            ]);
        
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username'], [
            'errorField' => 'error',
            'message' => 'Username is already taken.',
        ]));

        return $rules;
    } 
    
    public function findAuth(\Cake\ORM\Query $query, array $options)
    {
        $query
            ->select(['id', 'username', 'password'])
            ->where(['Users.status' => 'Active', 'Users.is_verified' => 0]);
            echo $query; exit;

        return $query;
    }
}
