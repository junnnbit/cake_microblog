<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Authentication\PasswordHasher\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string|null $firstname
 * @property string|null $middlename
 * @property string|null $lastname
 * @property string|null $nickname
 * @property string|null $gender
 * @property \Cake\I18n\FrozenDate|null $birthdate
 * @property string|null $location
 * @property string|null $contact_num
 * @property string|null $email
 * @property string|null $username
 * @property string|null $password
 * @property string|null $bio
 * @property string|null $profile_pic
 * @property string|null $header_pic
 * @property string|null $status
 * @property bool|null $is_private
 * @property string|null $hash_code
 * @property bool|null $is_verified
 * @property \Cake\I18n\FrozenTime|null $date_verified
 * @property \Cake\I18n\FrozenTime|null $date_modified
 * @property bool|null $is_deleted
 * @property \Cake\I18n\FrozenTime|null $date_deleted
 * @property \Cake\I18n\FrozenTime|null $date_created
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'middlename' => true,
        'lastname' => true,
        'nickname' => true,
        'gender' => true,
        'birthdate' => true,
        'location' => true,
        'contact_num' => true,
        'email' => true,
        'username' => true,
        'password' => true,
        'bio' => true,
        'profile_pic' => true,
        'header_pic' => true,
        'status' => true,
        'is_private' => true,
        'hash_code' => true,
        'is_verified' => true,
        'date_verified' => true,
        'date_modified' => true,
        'is_deleted' => true,
        'date_deleted' => true,
        'date_created' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
            // exit;
        }
    }
}
