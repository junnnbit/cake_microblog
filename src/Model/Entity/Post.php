<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property int $posts_id
 * @property int $users_id
 * @property string|null $content
 * @property \Cake\I18n\FrozenTime|null $date_modified
 * @property bool|null $is_deleted
 * @property \Cake\I18n\FrozenTime|null $date_deleted
 * @property \Cake\I18n\FrozenTime|null $date_created
 *
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Image[] $images
 * @property \App\Model\Entity\Notification[] $notifications
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'posts_id' => true,
        'users_id' => true,
        'content' => true,
        'date_modified' => true,
        'is_deleted' => true,
        'date_deleted' => true,
        'date_created' => true,
        'post' => true,
        'user' => true,
        'images' => true,
        'notifications' => true,
    ];
}
