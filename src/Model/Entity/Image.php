<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Image Entity
 *
 * @property int $id
 * @property int|null $post_id
 * @property int|null $comment_id
 * @property string|null $image
 * @property bool|null $is_deleted
 * @property \Cake\I18n\FrozenTime|null $date_deleted
 * @property \Cake\I18n\FrozenTime|null $date_created
 *
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\Comment $comment
 */
class Image extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post_id' => true,
        'comment_id' => true,
        'image' => true,
        'is_deleted' => true,
        'date_deleted' => true,
        'date_created' => true,
        'post' => true,
        'comment' => true,
    ];
}
