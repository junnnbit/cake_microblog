<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;

date_default_timezone_set('Asia/Hong_Kong');

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function display(string ...$path): ?Response
    {
        if (!$path) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            return $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function index()
    {
        $this->loadModel('Posts');
        $this->loadModel('FollowersFollowing');
        $this->loadModel('LikedPosts');
        $this->loadModel('Images');
        $this->loadModel('Comments');
        
        $this->paginate = [
            'maxLimit' => 2,
            'order' => ['date_created' => 'DESC']
        ];
        $user = $this->request->getSession()->read('Auth');

        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();
        $followers = $this->FollowersFollowing->find('all')->where(['following_id' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$user['id']];
        if (!empty($following)) {
            foreach ($following as $ff) {
                array_push($usersArray, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();

        $postsQuery = $this->Posts->find()->select(['active_user_id' => $user['id']])->select($this->Posts)->where(['Posts1.users_id =' => $user['id'], 'Posts1.is_deleted' => 0]);
        $postsQuery->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['post'] = ($row['posts_id'] === null ? [] : $this->Posts->find('all')->where(['Posts1.id' => intval($row['posts_id'])])->contain('Users')->first());
                
                if (!empty($row['post'])) {
                    $row['post']['images'] = $this->Images->find('all')->where(['post_id' => intval($row['posts_id']), 'is_deleted' => false]);
                }
                $row['images'] = $this->Images->find('all')->where(['post_id' => intval($row['id']), 'is_deleted' => false])->all();
                $row['user'] = $this->Users->find('all')->where(['id' => intval($row['users_id'])])->first();
                $row['liked'] =  $this->LikedPosts->find()->select(['id', 'users_id', 'posts_id', 'is_deleted'])->where(['posts_id' => intval($row['id']), 'users_id'=>intval($row['active_user_id']), 'is_deleted' => false])->first();              
                $row['liked_count'] = $this->LikedPosts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                $row['comment_count'] = $this->Comments->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                $row['shared_count'] = $this->Posts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 

                if (!empty($row['liked'])) {
                    $row['is_liked'] = $row['liked']['id'] === null ? 0 : 1;
                } else {
                    $row['is_liked'] = 0; 
                }
                return $row;
            });
        });
        
        $posts = $this->paginate($postsQuery);

        $this->set(compact('user', 'users', 'posts', 'following', 'followers', 'postsQuery'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'posts' => 'posts', 'following' => 'following', 'followers' => 'followers', 'postsQuery' => 'postsQuery']);
    }

    function user() {
        $id = base64_decode(base64_decode(base64_decode($this->request->getAttribute('params')['id'])));

        $this->loadModel('Posts');
        $this->loadModel('FollowersFollowing');
        $this->loadModel('LikedPosts');
        $this->loadModel('Images');
        $this->loadModel('Comments');
        
        $this->paginate = [
            'maxLimit' => 2,
            'order' => ['date_created' => 'DESC'],
            'contain' => ['Posts', 'Users']
        ];
        $activeUser = $this->request->getSession()->read('Auth');

        $user = $this->Users->find('all')->where(['id =' => $id])->first()->toArray();
        $activeUserFollowing = $this->FollowersFollowing->find('all')->where(['follower_id =' => $activeUser['id'], 'is_deleted' => false])->all();

        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();
        $followers = $this->FollowersFollowing->find('all')->where(['following_id' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$activeUser['id'], $user['id']];
        $activeUserFollowingIds = [];
        if (!empty($activeUserFollowing)) {
            foreach ($activeUserFollowing as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($activeUserFollowingIds, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();

        $postsQuery = $this->Posts->find()->select(['active_user_id' => $activeUser['id']])->select($this->Posts)->where(['Posts1.users_id =' => $user['id'], 'Posts1.is_deleted' => 0]);
        $postsQuery->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['post'] = ($row['posts_id'] === null ? [] : $this->Posts->find('all')->where(['Posts1.id' => intval($row['posts_id'])])->contain('Users')->first());
                
                if (!empty($row['post'])) {
                    $row['post']['images'] = $this->Images->find('all')->where(['post_id' => intval($row['posts_id']), 'is_deleted' => false]);
                }
                $row['images'] = $this->Images->find('all')->where(['post_id' => intval($row['id']), 'is_deleted' => false])->all();
                $row['user'] = $this->Users->find('all')->where(['id' => intval($row['users_id'])])->first();
                $row['liked'] =  $this->LikedPosts->find()->select(['id', 'users_id', 'posts_id', 'is_deleted'])->where(['posts_id' => intval($row['id']), 'users_id'=>intval($row['active_user_id']), 'is_deleted' => false])->first();              
                $row['liked_count'] = $this->LikedPosts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                $row['comment_count'] = $this->Comments->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                $row['shared_count'] = $this->Posts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                
                if (!empty($row['liked'])) {
                    $row['is_liked'] = $row['liked']['id'] === null ? 0 : 1;
                } else {
                    $row['is_liked'] = 0; 
                }
                return $row;
            });
        });
        
        $posts = $this->paginate($postsQuery);

        $this->set(compact('user', 'users', 'posts', 'following', 'followers', 'postsQuery', 'activeUser', 'activeUserFollowingIds'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'posts' => 'posts', 'following' => 'following', 'followers' => 'followers', 'postsQuery' => 'postsQuery', 'activeUser' => 'activeUser', 'activeUserFollowingIds' => 'activeUserFollowingIds']);
    }

    public function edit()
    {   
        $this->autoRender = false;
        $user = $this->Users->newEntity($this->request->getData());
        $user->id = $this->request->getData('id');
        $errors = $user->getErrors();
        
        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is(['patch', 'post', 'put'])) {
                
                if (!empty($this->request->getData('profile_pic_raw'))) {
                    $imageName = '(' . date('mdYHis') . ')' . $this->request->getData('profile_pic_raw')->getClientFilename();
                    $targetPath = WWW_ROOT.'img/profile_pics'.DS.$imageName;
                    $this->request->getData('profile_pic_raw')->moveTo($targetPath);

                    $user['profile_pic'] = $imageName;
                    $data = [
                        'id' => $user['id'],
                        'profile_pic' => $user['profile_pic']
                    ];
                } else {
                    $data = $this->request->getData();
                }

                $userData = $this->request->getData();
                $user = $this->Users->get($userData['id']);
                $user = $this->Users->patchEntity($user, $data);
                $user->date_modified = date("Y-m-d H:i:s");

                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The profile has been updated.'));
                    $selectNewProfile = $this->Users->find('all')->where(['id' => $userData['id']])->first();
                    if ($this->request->getSession()->read('Auth')['id'] === $selectNewProfile['id']) {
                        $this->request->getSession()->write('Auth', $selectNewProfile);
                    }
                    exit(json_encode(['error' => null, 'response' => true]));
                    
                } else {
                    $newError = $user->getErrors();
                    if ($newError) {
                        echo json_encode(['error' => $newError, 'response' => false]);
                        exit;
                    } else {
                        $this->Flash->error(__('The profile could not be updated. Please, try again.'));
                        exit(json_encode(['error' => null, 'response' => false]));
                    }
                    
                }
            }
        }
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    function search() {
            $searchKey = $this->request->getQuery('key');
            
            $this->loadModel('FollowersFollowing');
            
            $this->paginate = [
                'maxLimit' => 5
            ];

            $user = $this->request->getSession()->read('Auth');
            
            $terms = '%' . $searchKey . '%';
            $searchQuery = $this->Users->find();
            $searchQuery->where(function ($exp, $query) use ($terms) {
                $conc = $query->func()->concat([
                   'firstname' => 'identifier', ' ',
                   'lastname' => 'identifier'
                ]);
                return $exp->or_([
                    'firstname LIKE' => $terms,
                    'lastname LIKE' => $terms, 
                    'nickname LIKE' => $terms,
                    'username LIKE' => $terms
                ])->like($conc, $terms);
            });
            $searchQuery->where(['is_verified' => true, 'id <>' => $user['id']])
                        ->order('rand()');

            $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();
            $followers = $this->FollowersFollowing->find('all')->where(['following_id' => $user['id'], 'is_deleted' => false])->all();

            $usersArray = [$user['id']];
            $followingArray = [];
            if (!empty($following)) {
                foreach ($following as $ff) {
                    array_push($usersArray, $ff['following_id']);
                    array_push($followingArray, $ff['following_id']);
                }
            }

            $followersArray = [];
            if (!empty($followers)) {
                foreach ($followers as $fr) {
                    array_push($followersArray, $fr['follower_id']);
                }
            }

            $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();

            $searchResult = $this->paginate($searchQuery);

            $this->set(compact('user', 'users', 'searchResult', 'followingArray', 'followersArray', 'searchKey'));
            $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'searchResult' => 'searchResult', 'followingArray' => 'followingArray', 'followersArray' => 'followersArray', 'searchKey' => 'searchKey']);
    }
    
    public function signup()
    {
        $this->autoRender = false;
        $user = $this->Users->newEntity($this->request->getData());
        $errors = $user->getErrors();
        
        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is('post')) {
                $code = md5(strval(rand(0,1000)) . $user['email']);
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user['hash_code'] = $code;
                $user['profile_pic'] = 'default.png';
                $user->date_created = date("Y-m-d H:i:s");

                if ($this->Users->save($user)) {

                    $link = 'http://mb.cakephp1.ynsdev.pw/verification/' . base64_encode($code);

                    $mailer = new Mailer();
                    $mailer->setTransport('mailtrap')
                           ->setFrom('junainamanangan.yns@gmail.com')
                           ->setTo($user['email'])
                           ->setSubject('YNS Social Activation Link')
                           ->setEmailFormat('both');
                    
                    $message = '<div id="' . rand(0, 2500) . '">'
                             .'Hello, ' . $user['firstname'] . ' ' . $user['lastname'] . '!'
                             . '<br/><br/>'
                             . 'Thank you for signing-up on YNS Social site! Please click on the "Verify Account!" button to validate your account for you to be able to officially log-in on your account.'
                             . '<br/><br/>'
                             . 'Thank you.'
                             . '<br/><br/>'
                             . '<a href="' . $link . '"><button class="btn btn-success">Verify Account!</button></a>'
                             . '<br/><br/>'
                             . '[' . date('H:i:s m/d/Y') . ']' . ' End of message.'
                             . '</div>';
                    

                    if ($mailer->deliver($message)) {
                        $this->Flash->success(__('Successfully registered! An email has been sent to validate and activate your account.'));
                        exit(json_encode(['error' => null, 'response' => true]));
                    } else {
                        $this->Flash->error(__('Account registration failed. Please, try again.'));
                        exit(json_encode(['error' => null, 'response' => false]));
                    }
                    
                } else {
                    $newError = $user->getErrors();
                    if ($newError) {
                        echo json_encode(['error' => $newError, 'response' => false]);
                        exit;
                    } else {
                        $this->Flash->error(__('Account registration failed. Please, try again.'));
                        exit(json_encode(['error' => null, 'response' => false]));
                    }
                    
                }
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function verification()
    {
        $urlParams = $this->request->getAttribute('params');
        $paramCode = $urlParams['code'];

        $code = base64_decode($paramCode);

        $user = $this->Users;
        $user = $user->find()->select(['id', 'is_verified'])->where(['hash_code' => $code])->first();

        if (!empty($user)) {
            if ($user['is_verified'] == 1) {
                $this->Flash->success(__('Account is already verified. Please log-in.'));
                return $this->redirect('/');
            } else {
                $update = $this->Users->query()->update()->set(['is_verified' => 1, 'date_verified' => date("Y-m-d h:i:s"), 'status' => 'Active']);
                if ($update->execute()) {
                    $user = $this->Users;
                    $user = $user->find()->select(['id', 'is_verified'])->where(['hash_code' => $code])->first();
                }
            }  
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);

    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['loginIndex', 'signup', 'verification']);
    }

    public function loginIndex()
    {
        $userLoggedIn = !empty($this->request->getSession()->read('Auth'));

        if ($userLoggedIn) {
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Users',
                'action' => 'index',
            ]);

            return $this->redirect($redirect);
        } else {
            $this->request->allowMethod(['get', 'post']);
            if ($this->request->is('post')) {
                $result = $this->Authentication->getResult();


                if ($result->isValid()) {
                    $identity = $this->Authentication->getIdentity();
                    $user = $identity->getOriginalData();

                    if($user['is_verified'] && $user['status'] === 'Active') {
                        $this->loadModel('UserLogs');
                        $userLogsData = [
                            'users_id' => $user['id'],
                            'description' => 'Logged In'
                        ];
        
                        $userLog = $this->UserLogs->newEntity($userLogsData);
                        $userLog = $this->UserLogs->patchEntity($userLog, $userLogsData);
                        $userLog->date_created = date("Y-m-d H:i:s");
                        
                        $this->UserLogs->save($userLog);
                        return $this->redirect('/home');
                    } else {
                        $this->Flash->error(__('Unverified account'));
                        $this->Authentication->logout();
                        return $this->redirect('/');
                    }
                    
                } else {
                    $this->Flash->error(__('Invalid username or password'));
                }
            }
        }
    }

    public function logout()
    {
        $identity = $this->Authentication->getIdentity();
        $user = $identity->getOriginalData();

        $this->loadModel('UserLogs');
        $userLogsData = [
            'users_id' => $user['id'],
            'description' => 'Logged Out'
        ];
        
        $userLog = $this->UserLogs->newEntity($userLogsData);
        $userLog = $this->UserLogs->patchEntity($userLog, $userLogsData);
        $userLog->date_created = date("Y-m-d H:i:s");
        
        $this->UserLogs->save($userLog);
        $this->Authentication->logout();
        return $this->redirect('/');
    }

    
}
