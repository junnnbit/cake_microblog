<?php
declare(strict_types=1);

namespace App\Controller;
date_default_timezone_set('Asia/Hong_Kong');

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {;
        $this->loadModel('Users');
        $this->loadModel('FollowersFollowing');
        $this->loadModel('LikedPosts');
        $this->loadModel('Images');
        $this->loadModel('Comments');
        $this->paginate = [
            'maxLimit' => 3,
            'order' => ['date_created' => 'DESC']
        ];
        $user = $this->request->getSession()->read('Auth');
        
        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$user['id']];
        if (!empty($following)) {
            foreach ($following as $ff) {
                array_push($usersArray, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();

        $postsQuery = $this->Posts->find()->select(['active_user_id' => $user['id']])->select($this->Posts)->where(['Posts1.users_id =' => $user['id'], 'Posts1.is_deleted' => 0]);
        $postsQuery->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['post'] = ($row['posts_id'] === null ? [] : $this->Posts->find('all')->where(['Posts1.id' => intval($row['posts_id'])])->contain('Users')->first());
                
                if (!empty($row['post'])) {
                    $row['post']['images'] = $this->Images->find('all')->where(['post_id' => intval($row['posts_id']), 'is_deleted' => false]);
                }
                $row['images'] = $this->Images->find('all')->where(['post_id' => intval($row['id']), 'is_deleted' => false])->all();
                $row['user'] = $this->Users->find('all')->where(['id' => intval($row['users_id'])])->first();
                $row['liked'] =  $this->LikedPosts->find()->select(['id', 'users_id', 'posts_id', 'is_deleted'])->where(['posts_id' => intval($row['id']), 'users_id'=>intval($row['active_user_id']), 'is_deleted' => false])->first(); 
                $row['liked_count'] = $this->LikedPosts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                $row['comment_count'] = $this->Comments->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                $row['shared_count'] = $this->Posts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
             
                if (!empty($row['liked'])) {
                    $row['is_liked'] = $row['liked']['id'] === null ? 0 : 1;
                } else {
                    $row['is_liked'] = 0; 
                }
                return $row;
            });
        });
        
        $posts = $this->paginate($postsQuery);

        $this->set(compact('user', 'users', 'posts'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'posts' => 'posts']);
    }

    public function add()
    {
        $this->autoRender = false;
        $this->loadModel('Images');
        $userId = $this->request->getSession()->read('Auth')['id'];
        $data = [
            'content' => $this->request->getData('content'),
            'users_id' => $userId,
            'images' => [],
            'posts_id' => empty($this->request->getData('posts_id')) ? null : $this->request->getData('posts_id')
        ];

        if (empty($this->request->getData()['action']) && $this->request->getData('images')[0]->getClientFilename() !== '') {

            $imagesRequests = $this->request->getData('images');

            for ($index = 0; $index < count($imagesRequests); $index++) {
                $data['images'][] = ['images' => $imagesRequests[$index]];
            }
        } 

        $entity = $this->Posts->newEntity($data, [
            'associated' => ['Images']
        ]);

        if (!empty($this->request->getData()['action']) && $this->request->getData()['action'] === 'Repost') {
            $errors = false;
            $entity->posts_id = $this->request->getData('posts_id');
        } else {
            $errors = $entity->getErrors();
        }

        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is('post')) {
                if (!empty($entity['images'])) {
                    $entity['images'] = [];
                    $data['images'] = [];
                    $imagesRequests = $this->request->getData('images');

                    for ($index = 0; $index < count($imagesRequests); $index++) {
                        $imageName = '(' . date('mdYHis') . ')' . $imagesRequests[$index]->getClientFilename();
                        $entity['images'][] = ['image' => $imageName];
                        $data['images'][] = ['image' => $imageName];
                        
                        $targetPath = WWW_ROOT.'img/posts_images'.DS.$imageName;
                        $imagesRequests[$index]->moveTo($targetPath);
                    }
                } 

                $post = $this->Posts->patchEntity($entity, $data, [
                    'associated' => ['Images']
                ]);

                $post->date_created = date("Y-m-d H:i:s");

                if ($this->Posts->save($post)) {
                    if (!empty($this->request->getData('posts_id'))) {
                        $sharedPostId = $this->request->getData('posts_id');
    
                        $postDeets = $this->Posts->find()->select(['id', 'users_id'])->where(['Posts1.id'=>$sharedPostId])->first();
                    
                        if ($postDeets['users_id'] !== $userId) {
                            $this->loadModel('Notifications');
    
                            $notificationData = [
                                'viewers_id' => $postDeets['users_id'],
                                'users_id' => $userId,
                                'post_id' => $postDeets['id'],
                                'content' => 'shared your post'
                            ];
    
                            $notifications = $this->Notifications->newEntity($notificationData);
                            $notifications = $this->Notifications->patchEntity($notifications, $notificationData);

                            $notifications->date_created = date("Y-m-d H:i:s");
    
                            $this->Notifications->save($notifications);
                        } 
                    } 
                    $this->Flash->success(__('The post has been posted.'));
                    exit(json_encode(['error' => null, 'response' => true]));
                } else {
                    $this->Flash->error(__('The post could not be posted. Please, try again.'));
                    exit(json_encode(['error' => null, 'response' => false]));
                }
            }
        }
    }

    public function edit()
    {
        $this->autoRender = false;
        $this->loadModel('Images');

        $data = [
            'content' => $this->request->getData('content'),
            'images' => [],
            'id' => $this->request->getData('posts_id')
        ];


        if ($this->request->getData('images')[0]->getClientFilename() !== '') {

            $imagesRequests = $this->request->getData('images');

            for ($index = 0; $index < count($imagesRequests); $index++) {
                $data['images'][] = ['images' => $imagesRequests[$index]];
            }
           
        } 

        $entity = $this->Posts->newEntity($data, [
            'associated' => ['Images']
        ]);

        $errors = $entity->getErrors();
        
        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is(['patch', 'post', 'put'])) {
                if (!empty($entity['images'])) {
                    $entity['images'] = [];
                    $data['images'] = [];
                    $imagesRequests = $this->request->getData('images');

                    for ($index = 0; $index < count($imagesRequests); $index++) {
                        $imageName = '(' . date('mdYHis') . ')' . $imagesRequests[$index]->getClientFilename();
                        $entity['images'][] = ['posts_id' => $this->request->getData('posts_id'), 'image' => $imageName, 'is_deleted' => false, 'id' => null];
                        $data['images'][] = ['posts_id' => $this->request->getData('posts_id'), 'image' => $imageName, 'is_deleted' => false, 'id' => null];
                        
                        $targetPath = WWW_ROOT.'img/posts_images'.DS.$imageName;
                        $imagesRequests[$index]->moveTo($targetPath);
                    }
                }

                $uploadedImagesId = [];
                if (!empty($this->request->getData('preloaded'))) {
                    $preLoaded = $this->request->getData('preloaded');
                    for ($index = 0; $index < count($preLoaded); $index++) {
                        if (strpos($preLoaded[$index], '-')) {
                            array_push($uploadedImagesId, explode('-', $preLoaded[$index])[1]);
                        }
                    }
                }

                $selectImages = $this->Images->find()->select(['id', 'image'])->where(['post_id' => $this->request->getData('posts_id'), 'is_deleted' => false])->all()->toArray();

                for ($index = 0; $index < count($selectImages); $index++) {
                    if (!in_array($selectImages[$index]['id'], $uploadedImagesId)) {
                        $imageName = $selectImages[$index]['image'];
                        $entity['images'][] = ['posts_id' => $this->request->getData('posts_id'), 'image' => $imageName, 'is_deleted' => true, 'id' => $selectImages[$index]['id']];
                        $data['images'][] = ['posts_id' => $this->request->getData('posts_id'), 'image' => $imageName, 'is_deleted' => true, 'id' => $selectImages[$index]['id']];
                    }
                }    
                $entity->id = $this->request->getData('posts_id');
                $post = $this->Posts->patchEntity($entity, $data, [
                    'associated' => ['Images']
                ]);

                $post->date_modified = date("Y-m-d H:i:s");

                if ($this->Posts->save($post)) {
                    $this->Flash->success(__('The post has been updated.'));
                    exit(json_encode(['error' => null, 'response' => true]));
                } else {
                    $this->Flash->error(__('The post could not be updated. Please, try again.'));
                    exit(json_encode(['error' => null, 'response' => false]));
                }
            }
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $post = $this->Posts->newEntity($this->request->getData());

        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post['id'] = $post['posts_id'];
            $post['posts_id'] = null;
            $query = $this->Posts->query()->update()->set(['is_deleted' => 1, 'date_deleted' => date("Y-m-d H:i:s")])->where(['id' => $post['id']]);

            if ($query->execute()) {
                $this->Flash->success(__('The post has been deleted.'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('The post could not be deleted. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }

    public function likePost()
    {
        if ($this->request->is('post')) {
            $this->loadModel('LikedPosts');

            $activeUserId = $this->request->getSession()->read('Auth')['id'];
            
            $likedPosts =  $this->LikedPosts->newEntity($this->request->getData());
            $likedPosts['users_id'] = $activeUserId;
            $likedPosts->date_created = date("Y-m-d H:i:s");

            if ($this->LikedPosts->save($likedPosts)) {
                $postId = $this->request->getData('posts_id');
                $postDeets = $this->Posts->find()->select(['id', 'users_id'])->where(['Posts1.id'=>$postId])->first();
                $countLikes = $this->LikedPosts->find()->where(['posts_id'=>$postId, 'users_id'=>$activeUserId])->count();
                    
                if ($postDeets['users_id'] !== $activeUserId && intval($countLikes) === 1) {
                    $this->loadModel('Notifications');
    
                    $notificationData = [
                        'viewers_id' => $postDeets['users_id'],
                        'users_id' => $activeUserId,
                        'post_id' => $postDeets['id'],
                        'content' => 'liked your post'
                    ];
    
                    $notifications = $this->Notifications->newEntity($notificationData);
                    $notifications = $this->Notifications->patchEntity($notifications, $notificationData);
                    $notifications->date_created = date("Y-m-d H:i:s");
                    
                    $this->Notifications->save($notifications);
                }
                $this->Flash->success(__('Liked!'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('Encountered a problem in liking the post. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }

    public function unlikePost()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('LikedPosts');

            $likedId = $this->request->getData()['id'];

            $query = $this->LikedPosts->query()->update()->set(['is_deleted' => true, 'date_deleted' => date("Y-m-d h:i:s")])->where(['id =' => $likedId, 'is_deleted =' => false]);

            if ($query->execute()) {
                $this->Flash->success(__('The post has been unliked.'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('The user could not be unliked. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }

}
