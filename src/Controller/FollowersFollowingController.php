<?php
declare(strict_types=1);

namespace App\Controller;
date_default_timezone_set('Asia/Hong_Kong');

/**
 * FollowersFollowing Controller
 *
 * @property \App\Model\Table\FollowersFollowingTable $FollowersFollowing
 * @method \App\Model\Entity\FollowersFollowing[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersFollowingController extends AppController
{
    public function add()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $activeUserId = $this->request->getSession()->read('Auth')['id'];
            
            $followersFollowing =  $this->FollowersFollowing->newEntity($this->request->getData());
            $followersFollowing['follower_id'] = $activeUserId;
            $followersFollowing->date_created = date("Y-m-d H:i:s");
            if ($this->FollowersFollowing->save($followersFollowing)) {
                $this->loadModel('Notifications');
    
                $notificationData = [
                    'viewers_id' => $this->request->getData('following_id'),
                    'users_id' => $activeUserId,
                    'content' => 'started following you'
                ];
    
                $notifications = $this->Notifications->newEntity($notificationData);
                $notifications = $this->Notifications->patchEntity($notifications, $notificationData);
                $notifications->date_created = date("Y-m-d H:i:s");

                $this->Notifications->save($notifications);

                $this->Flash->success(__('Successfully followed.'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('Encountered a problem in following. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }

    public function following() {
        $this->loadModel('Users');
        
        $this->paginate = [
            'maxLimit' => 5,
            'contain' => ['Users' =>['foreignKey' => 'following_id']]
        ];
        $user = $this->request->getSession()->read('Auth');

        $followingQ = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$user['id']];
        $followingArray = [];
        if (!empty($followingQ)) {
            foreach ($followingQ as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($followingArray, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();
        
        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'FollowersFollowing.is_deleted' => false])->order('rand()');

        $followingUsers = $this->paginate($following);
        $this->set(compact('user', 'users', 'followingUsers', 'following'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'followingUsers' => 'followingUsers', 'following' => 'following']);
    }

    public function followers() {
        $this->loadModel('Users');
        
        $this->paginate = [
            'maxLimit' => 5,
            'contain' => ['Users' =>['foreignKey' => 'follower_id']]
        ];
        $user = $this->request->getSession()->read('Auth');

        $followingQ = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$user['id']];
        $followingArray = [];
        if (!empty($followingQ)) {
            foreach ($followingQ as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($followingArray, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();
        
        $followers = $this->FollowersFollowing->find('all')->where(['following_id =' => $user['id'], 'FollowersFollowing.is_deleted' => false])->order('rand()')
        ->join([
            'table' => 'users',
            'alias' => 'u',
            'type' => 'INNER',
            'conditions' => 'u.id = FollowersFollowing.follower_id',
        ]);

        $followersUsers = $this->paginate($followers);
        $this->set(compact('user', 'users', 'followersUsers', 'followers', 'followingArray'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'followersUsers' => 'followersUsers', 'followers' => 'followers', 'followingArray' => 'followingArray']);
    }

    public function userFollowing() {
        $id = base64_decode(base64_decode(base64_decode($this->request->getAttribute('params')['id'])));

        $this->loadModel('Users');
        
        $this->paginate = [
            'maxLimit' => 5,
            'contain' => ['Users' =>['foreignKey' => 'following_id']]
        ];
        $activeUser = $this->request->getSession()->read('Auth');
        $activeUserFollowingQ = $this->FollowersFollowing->find('all')->where(['follower_id =' => $activeUser['id'], 'is_deleted' => false])->all();

        $user = $this->Users->find('all')->where(['id =' => $id])->first()->toArray();
        
        $usersArray = [$activeUser['id'], $user['id']];
        $activeUserFollowing = [];
        if (!empty($activeUserFollowingQ)) {
            foreach ($activeUserFollowingQ as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($activeUserFollowing, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();
        
        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'FollowersFollowing.is_deleted' => false])->order('rand()');
        $followingUsers = $this->paginate($following);
        $this->set(compact('user', 'users', 'followingUsers', 'following', 'activeUserFollowing', 'activeUser'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'followingUsers' => 'followingUsers', 'following' => 'following', 'activeUserFollowing' => 'activeUserFollowing', 'activeUser' => 'activeUser']);
    }

    public function userFollowers() {
        $id = base64_decode(base64_decode(base64_decode($this->request->getAttribute('params')['id'])));

        $this->loadModel('Users');
        
        $this->paginate = [
            'maxLimit' => 5,
            'contain' => ['Users' =>['foreignKey' => 'follower_id']]
        ];
        
        $activeUser = $this->request->getSession()->read('Auth');
        $activeUserFollowingQ = $this->FollowersFollowing->find('all')->where(['follower_id =' => $activeUser['id'], 'is_deleted' => false])->all();
       
        $user = $this->Users->find('all')->where(['id =' => $id])->first()->toArray();

        $usersArray = [$activeUser['id'], $user['id']];
        $activeUserFollowing = [];
        if (!empty($activeUserFollowingQ)) {
            foreach ($activeUserFollowingQ as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($activeUserFollowing, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();
        
        $followers = $this->FollowersFollowing->find('all')->where(['following_id =' => $user['id'], 'FollowersFollowing.is_deleted' => false])->order('rand()');

        $followersUsers = $this->paginate($followers);
        $this->set(compact('user', 'users', 'followersUsers', 'followers', 'activeUserFollowing', 'activeUser'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'followersUsers' => 'followersUsers', 'followers' => 'followers', 'activeUserFollowing' => 'activeUserFollowing', 'activeUser' => 'activeUser']);
    }

    public function unfollow() { 
        $following = $this->FollowersFollowing->newEntity($this->request->getData());

        if ($this->request->is(['patch', 'post', 'put'])) {
            $following = $this->FollowersFollowing->patchEntity($following, $this->request->getData());

            $query = $this->FollowersFollowing->query()->update()->set(['is_deleted' => true, 'date_deleted' => date("Y-m-d h:i:s")])->where(['following_id =' => $following['following_id'], 'follower_id =' => $following['follower_id'], 'is_deleted =' => false]);

            if ($query->execute()) {
                $this->Flash->success(__('The user has been unfollowed.'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('The user could not be unfollowed. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }

}
