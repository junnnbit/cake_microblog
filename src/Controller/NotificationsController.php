<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Notifications Controller
 *
 * @property \App\Model\Table\NotificationsTable $Notifications
 * @method \App\Model\Entity\Notification[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NotificationsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->loadModel('Users');
        $this->loadModel('FollowersFollowing');
        $this->loadModel('Posts');

        $this->paginate = [
            'maxLimit' => 5,
            'order' => ['date_created' => 'DESC'],
        ];

        $activeUserId = $this->request->getSession()->read('Auth')['id'];

        $user = $this->request->getSession()->read('Auth');
        $following = $this->FollowersFollowing->find('all')->where(['follower_id =' => $user['id'], 'is_deleted' => false])->all();

        $usersArray = [$user['id']];
        if (!empty($following)) {
            foreach ($following as $ff) {
                array_push($usersArray, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();


        $notificationsData = $this->Notifications->find('all')->where(['viewers_id' => $activeUserId]);
        $notificationsData->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['user'] = $this->Users->find('all')->where(['id' => intval($row['users_id'])])->first();
                $row['post'] = ($row['post_id'] === null ? [] : $this->Posts->find('all')->where(['Posts1.id' => intval($row['post_id'])])->contain('Users')->first());
                
                return $row;
            });
        });
        $notifications = $this->paginate($notificationsData);

        $this->set(compact('user', 'users', 'notifications'));
        $this->set('_serialize', ['user' => 'user', 'users' => 'users', 'notifications' => 'notifications']);
    }

}
