<?php
declare(strict_types=1);

namespace App\Controller;
date_default_timezone_set('Asia/Hong_Kong');


/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $id = base64_decode(base64_decode(base64_decode($this->request->getAttribute('params')['id'])));

        $this->loadModel('Users');
        $this->loadModel('Posts');
        $this->loadModel('FollowersFollowing');
        $this->loadModel('LikedPosts');
        $this->loadModel('Images');
        $this->paginate = [
            'maxLimit' => 2,
            'order' => ['date_created' => 'DESC'],
            'contain' => ['Users']
        ];
        $activeUser = $this->request->getSession()->read('Auth');
        $activeUserFollowing = $this->FollowersFollowing->find('all')->where(['follower_id =' => $activeUser['id'], 'is_deleted' => false])->all();

        $usersArray = [$activeUser['id']];
        $activeUserFollowingIds = [];
        if (!empty($activeUserFollowing)) {
            foreach ($activeUserFollowing as $ff) {
                array_push($usersArray, $ff['following_id']);
                array_push($activeUserFollowingIds, $ff['following_id']);
            }
        }

        $users = $this->Users->find('all')->where(['id NOT IN' => $usersArray, 'is_verified =' => 1, 'status' => 'Active'])->order('rand()')->limit(5)->all();
        
        $postQuery = $this->Posts->find()->where(['Posts1.id' => $id, 'Posts1.is_deleted' => 0]);
        $postQuery->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['post'] = ($row['posts_id'] === null ? [] : $this->Posts->find('all')->where(['Posts1.id' => intval($row['posts_id'])])->contain('Users')->first());
                
                if (!empty($row['post'])) {
                    $row['post']['images'] = $this->Images->find('all')->where(['post_id' => intval($row['posts_id']), 'is_deleted' => false]);
                }
                $row['images'] = $this->Images->find('all')->where(['post_id' => intval($row['id']), 'is_deleted' => false])->all();
                $row['user'] = $this->Users->find('all')->where(['id' => intval($row['users_id'])])->first();
                $row['liked'] =  $this->LikedPosts->find()->select(['id', 'users_id', 'posts_id', 'is_deleted'])->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->first();              
                $row['liked_count'] = $this->LikedPosts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count();           
                $row['comment_count'] = $this->Comments->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                $row['shared_count'] = $this->Posts->find()->where(['posts_id' => intval($row['id']), 'is_deleted' => false])->count(); 
                
                if (!empty($row['liked'])) {
                    $row['is_liked'] = $row['liked']['id'] === null ? 0 : 1;
                } else {
                    $row['is_liked'] = 0; 
                }
                return $row;
            });
        });

        $post = $postQuery->first();

        $commentsQuery = $this->Comments->find('all')->where(['Comments.posts_id' => $id, 'Comments.is_deleted' => false]);
        $commentsQuery->formatResults(function (\Cake\Collection\CollectionInterface $results) {
            return $results->map(function ($row) {
                $row['images'] = $this->Images->find('all')->where(['comment_id' => intval($row['id']), 'is_deleted' => false])->all();
                return $row;
            });
        });
        $comments = $this->paginate($commentsQuery);

        $this->set(compact('users', 'activeUser', 'activeUserFollowingIds', 'post', 'comments'));
        $this->set('_serialize', ['users' => 'users', 'activeUser' => 'activeUser', 'activeUserFollowingIds' => 'activeUserFollowingIds', 'post' => 'post', 'comments' => 'comments']);
    }
    
    public function add()
    {
        $this->autoRender = false;
        $this->loadModel('Images');
        $this->loadModel('Posts');
        
        $userId = $this->request->getSession()->read('Auth')['id'];
        $data = [
            'content' => $this->request->getData('content'),
            'users_id' => $userId,
            'images' => [],
            'posts_id' => $this->request->getData('posts_id')
        ];

        if (empty($this->request->getData()['action']) && $this->request->getData('images')[0]->getClientFilename() !== '') {

            $imagesRequests = $this->request->getData('images');

            for ($index = 0; $index < count($imagesRequests); $index++) {
                $data['images'][] = ['images' => $imagesRequests[$index]];
            }
        } 

        $entity = $this->Comments->newEntity($data, [
            'associated' => ['Images']
        ]);

        $errors = $entity->getErrors();

        if($errors) {
            echo json_encode(['error' => $errors, 'response' => false]);
            exit;
        } else {
            if ($this->request->is('post')) {
                if (!empty($entity['images'])) {
                    $entity['images'] = [];
                    $data['images'] = [];
                    $imagesRequests = $this->request->getData('images');

                    for ($index = 0; $index < count($imagesRequests); $index++) {
                        $imageName = '(' . date('mdYHis') . ')' . $imagesRequests[$index]->getClientFilename();
                        $entity['images'][] = ['image' => $imageName];
                        $data['images'][] = ['image' => $imageName];
                        
                        $targetPath = WWW_ROOT.'img/comments_images'.DS.$imageName;
                        $imagesRequests[$index]->moveTo($targetPath);
                    }
                } 

                $comment = $this->Comments->patchEntity($entity, $data, [
                    'associated' => ['Images']
                ]);

                $comment->date_created = date("Y-m-d H:i:s");

                if ($this->Comments->save($comment)) {
                    $postDeets = $this->Posts->find()->select(['id', 'users_id'])->where(['Posts1.id'=>$this->request->getData('posts_id')])->first();
                
                    if ($postDeets['users_id'] !== $userId) {
                        $this->loadModel('Notifications');

                        $notificationData = [
                            'viewers_id' => $postDeets['users_id'],
                            'users_id' => $userId,
                            'post_id' => $postDeets['id'],
                            'content' => 'commented on your post'
                        ];

                        $notifications = $this->Notifications->newEntity($notificationData);
                        $notifications = $this->Notifications->patchEntity($notifications, $notificationData);
                        $notifications->date_created = date("Y-m-d H:i:s");

                        $this->Notifications->save($notifications);
                    } 
                    $this->Flash->success(__('The comment has been posted.'));
                    exit(json_encode(['error' => null, 'response' => true]));
                } else {
                    $this->Flash->error(__('The comment could not be posted. Please, try again.'));
                    exit(json_encode(['error' => null, 'response' => false]));
                }
            }
        }
    }

    public function delete()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->request->getData();
            $query = $this->Comments->query()->update()->set(['is_deleted' => 1, 'date_deleted' => date("Y-m-d H:i:s")])->where(['id' => $comment['id']]);

            if ($query->execute()) {
                $this->Flash->success(__('The comment has been deleted.'));
                exit(json_encode(['error' => null, 'response' => true]));
            } else {
                $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
                exit(json_encode(['error' => null, 'response' => false]));
            }
        }
    }
}
